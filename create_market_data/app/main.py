"""
This is the implementation of data preparation for sklearn
"""
import pandas as pd
from datetime import timedelta, datetime
import datetime
import DatastreamDSWS as ds
import DatastreamDSWS as dsws
import app.QUANTFUNCTIONSAUG as qnt


import numpy as np
from scipy.optimize import minimize
import math
# ##### OPTIMISATION OBJECT AND FUNCTION   ###############
# This one creates the optimisation object
def objfunc(par, F, K, time, MKT):
    sum_sq_diff = 0
    # if K[0]<=0:
    #    shift(F,K)
    for j in range(len(K)):
        if MKT[j] == 0:
            diff = 0
        elif F == K[j]:
            V = (F * K[j]) ** ((1 - par[1]) / 2.)
            logFK = math.log(F / K[j])
            A = 1 + (((1 - par[1]) ** 2 * par[0] ** 2) / (24. * (V ** 2)) + (par[0] * par[1] * par[3] * par[2]) / (
                        4. * V) + ((par[3] ** 2) * (2 - 3 * (par[2] ** 2)) / 24.)) * time
            B = 1 + (1 / 24.) * (((1 - par[1]) * logFK) ** 2) + (1 / 1920.) * (((1 - par[1]) * logFK) ** 4)
            VOL = (par[0] / V) * A
            diff = VOL - MKT[j]
        elif F != K[j]:
            V = (F * K[j]) ** ((1 - par[1]) / 2.)
            logFK = math.log(F / K[j])
            z = (par[3] / par[0]) * V * logFK
            x = math.log((math.sqrt(1 - 2 * par[2] * z + z ** 2) + z - par[2]) / (1 - par[2]))
            A = 1 + (((1 - par[1]) ** 2 * par[0] ** 2) / (24. * (V ** 2)) + (par[0] * par[1] * par[3] * par[2]) / (
                        4. * V) + ((par[3] ** 2) * (2 - 3 * (par[2] ** 2)) / 24.)) * time
            B = 1 + (1 / 24.) * (((1 - par[1]) * logFK) ** 2) + (1 / 1920.) * (((1 - par[1]) * logFK) ** 4)
            VOL = (par[3] * logFK * A) / (x * B)
            diff = VOL - MKT[j]
        sum_sq_diff = sum_sq_diff + diff ** 2
        obj = math.sqrt(sum_sq_diff)
    return obj

    # This does the actual optimisation of parameters on the above OBJECT
    # NOTE THE bnds - boundary conditions, where can SET BETA = 1


def calibration(starting_par, F, K, time, MKT, fixbeta):
    x0 = starting_par
    bnds = ((0.001, None), (fixbeta, 1), (-0.999, 0.999), (0.001, None))  # FIX BETA TO 1 HERE
    res = minimize(objfunc, x0, (F, K, time, MKT), bounds=bnds,
                   method='SLSQP')  # for a constrained minimization of multivariate scalar functions
    alpha = res.x[0]
    beta = res.x[1]
    rho = res.x[2]
    nu = res.x[3]
    paramlist = [alpha, beta, rho, nu]
    return paramlist


def bulk_sabr_fit_refinitiv(df, gentenors, genvoltenorcodes, times, genKlist, volcode, Salpha,Srho,Snu):

    voltenorcode = genvoltenorcodes
    
    dfoutput=pd.DataFrame()
    
    for y in range(0, len(gentenors)):
        SABR_params_list=[]
        
        for i in range(0,len(df)):
    
            colstring=gentenors[y]+'_fwd'
            fwd = df[colstring][i]
            
            bbgcodelist=[]
            fitting_vollist=[]
            fitting_strikelist=[]
            dfout=pd.DataFrame()
            
            for j in range(0, len(genKlist)):
                
                bbgcodelist.append(volcode+str(genKlist[j]))
                
                fitting_vollist.append(df[bbgcodelist[j], voltenorcode[y]] [i]/100 )
                abs_strike=genKlist[j]/100 * df['SpotPrice','PI'][i]
                fitting_strikelist.append(abs_strike)
                
                df_list=list(zip(fitting_strikelist,fitting_vollist))
                dfout=pd.DataFrame(df_list, columns=['Strike','Vol'])
            
            #print(dfout)
            #print(fwd) 
            print(df.index[i])
            SABR_params=FitSABRparams(dfout, fwd, times[y], 100, 10000, Salpha,Srho,Snu  )
            SABR_params_list.append(SABR_params)
            print(SABR_params)
        headers= [ gentenors[y]+'_alpha', gentenors[y]+'_beta', gentenors[y]+'_rho' , gentenors[y]+'_vv' ]
        SABR_params_df=pd.DataFrame(SABR_params_list, columns=headers)       
    
        dfoutput = pd.concat( [dfoutput, SABR_params_df], axis=1)
    
    dfoutput['Date'] = df.index
    dfoutput.set_index('Date', inplace = True, drop = True)

    return dfoutput


def strikefilter(Kmin, Kmax, K, MktV):
    vols = []
    for k in range(len(K)):
        if Kmin <= K[k] <= Kmax:
            vols.append(MktV[k])
        else:
            vols.append(0)
    return vols


#Gives SABR params for a single Expiry Fit - df needs 2 cols 'Strike' & 'Vol'
def FitSABRparams(df, fwd, tyrs, Kmin, Kmax, Salpha,Srho,Snu  ):

    starting_guess = np.array([Salpha, 1.0, Srho, Snu])
    alpha = starting_guess[0]
    beta = starting_guess[1]
    rho = starting_guess[2]
    nu = starting_guess[3]
    fwdvalue = fwd
    timevalue = tyrs
    FixBetato1 = 1  # lower bound for beta in ( X, 1) so if set to 1 then Beta =1 fixed, if set to 0 then fit Beta

    df["MktVolAdj"] = 0
    df["MktVolAdj"] = strikefilter(Kmin, Kmax, df["Strike"], df["Vol"])
    ##############      CALIBRATE STANDARD SABR TO ATM STRIKES     #################
    # define list to hold calibration output
    fitresult = []
    fitresult = calibration(starting_guess, fwdvalue, df["Strike"], timevalue, df["MktVolAdj"], FixBetato1)
    
    return fitresult

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("create_market_data",level=logging.INFO)

class DSWSAdapter:

    def __init__(self, username, password):
        self.username = username
        self.password = password
        DSWSAdapter.ds = dsws.Datastream(username=self.username, password=self.password)
        # print(DSWSAdapter.ds.tokenResp)

    def GetPriceHistForTicker(self, tickerList, fieldList, startDate):
        df = DSWSAdapter.ds.get_data(tickers=tickerList, fields=fieldList, start=startDate, end='-5D')
        return pd.DataFrame(df)

    def GetAnalyticsForTicker(self, tickerList, fieldList):
        df = DSWSAdapter.ds.get_data(tickers=tickerList, fields=fieldList, kind=0)
        return pd.DataFrame(df)
    

class GetMarketData():
    def __init__(self, name, use_csv, Refin_data_df, use_vol_csv, vol_csv_path, startdate, Divpoints, gen_tenors, gen_vol_tenor_codes, gen_times, gen_strikes, RFrate_code, ticker, volticker):
        self.name=name
        self.use_csv=use_csv
        self.Refin_data_df=Refin_data_df
        self.use_vol_csv=use_vol_csv
        self.vol_csv_path=vol_csv_path              
        self.startdate=startdate
        self.Divpoints=Divpoints
        self.gen_tenors=gen_tenors
        self.gen_times=gen_times
        self.gen_strikes=gen_strikes
        self.RFrate_code=RFrate_code      
        self.gen_vol_tenor_codes=gen_vol_tenor_codes
        self.ticker=ticker
        self.volticker=volticker
        
    #CREATE MARKETDATA DATAFRAME
    def generate_market_data(self):  
        
        if self.use_csv==1:
            
            mdata = self.Refin_data_df.copy()
            mdata.index = pd.to_datetime(mdata.index, format='%Y-%m-%d')#'%d/%m/%Y')
            mdata.dropna(inplace=True)
            
            #RENAME SPOT COLUMN
            mdata = mdata.rename(columns={self.ticker: 'SpotPrice'})
            #FILTER TO START DATE AND SET START_SPOT
            mdata=mdata[self.startdate:]
            self.starting_spot= mdata['SpotPrice','PI'][self.startdate]
            
            #ADD DIVYIELD TO DF
            div_df = mdata.droplevel(1, axis=1).copy()
            div_df = div_df.loc[:, 'SpotPrice']
            mdata['DivYield'] = qnt.calcdivyield(div_df, self.Divpoints )
            
            #CALC GENERIC DATE FORWARDS TO DF
            fwd_df = mdata.droplevel(1, axis=1).copy()           
            for i in range(0,len(self.gen_tenors)):
                mdata[self.gen_tenors[i]+'_fwd']=fwd_df.apply(lambda x: qnt.fwdpricecalc( x['SpotPrice'],   x[self.RFrate_code + self.gen_tenors[i]]/100   ,  x['DivYield'], self.gen_times[i]), axis=1)
          
            #BUILD THE YIELD CURVES DF
            tmp_yield_df = mdata.droplevel(1, axis=1).copy()
            yield_bbgcode_list=[]
            for i in range(0,len(self.gen_tenors)):
                yield_bbgcode_list.append(self.RFrate_code + self.gen_tenors[i])
            self.yield_df = tmp_yield_df.loc[:, yield_bbgcode_list ]
            
            #COPY THE MDATA FRAME BEFORE DROPLEVEL
            self.vdata = mdata.copy() 
        
            mdata = mdata.droplevel(1, axis=1) 
            
            #BUILD THE SABR PARAMETERS DF
            if self.use_vol_csv==1:
                self.SABR_df = pd.read_csv(self.vol_csv_path, index_col=[0])
                self.SABR_df.index = pd.to_datetime(self.SABR_df.index, format='%Y-%m-%d')
                #filter down for speed while testing - NOTE THIS SEEMS TO BE MM/DD/YY not sure why
                self.SABR_df=self.SABR_df[self.startdate:]
                
            else:
                Sabr_initial_guess=[0.3, -0.7, 1.5]
                self.SABR_df = bulk_sabr_fit_refinitiv(self.vdata, self.gen_tenors, self.gen_vol_tenor_codes, self.gen_times, self.gen_strikes,  self.volticker, Sabr_initial_guess[0],Sabr_initial_guess[1],Sabr_initial_guess[2] )
                #print(self.vdata)
                self.vdata.to_csv("/data/VDATA_DF.csv")
                #print("In the SABR bit")
                #print(self.SABR_df)
        else:
             pass  
        return mdata    
    

class CreateMarketData(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="CreateMarketData")
        """ Initialize all the required constansts and data her """

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            
            ticker='DJES50I'
            volticker='GXEMONEY'
            startdate='01/02/2007'
            marketdata_startdate ='2007-01-02'
            
            #REFINITIV CODES
            fieldList = ['PI','O1','O3','OY','IR']
            underlyinglist = 'DJES50I,GXEMONEY80,GXEMONEY90,GXEMONEY95,GXEMONEY100,GXEMONEY105,GXEMONEY110,GXEMONEY115,OIEUR1M,OIEUR3M,OIEUR1Y'
            
            
            #ENTER DIVIDENDS
            Divpoints={'2007': 146.5,'2008': 158.6,'2009': 115.7,'2010': 112.7,'2011': 124.3,'2012': 115.6,'2013': 109.8,
                       '2014': 114.1,'2015': 114.9,'2016': 118.4,'2017':116.9 ,'2018': 125.6, '2019': 122.1, '2020': 86.4 }
            
            gen_tenors=["1M","3M","1Y"]
            gen_vol_tenor_codes = ['O1', 'O3', 'OY']
            gen_times=[0.084,0.25,1]
            gen_strikes=[80, 90, 95, 100, 105, 110, 115]
            
            RFrate_code='OIEUR'
            
            #TO PICK UP THE OUTPUT FROM COMPONENT1
            #Config_file = "C:/Users/ralphm/DemoPython/Outputs/StrategyDataFile.csv"

            base_strat_file = "/data/StrategyDataFile.csv"
            base_mdata_file = "/data/Refinitivbase.csv"
            
            #TO PICK UP THE OTHER INPUT FILES IF USING CSVs
            #mdata_file = "C:/Users/ralphm/DemoPython/Inputs/MDATA_DF.csv"
            SABRdata_file = "data/SABR_DF.csv"
            
            
            output_dir = "/data/"

            # LOAD IN PRICES FROM REFINITIV
            if __name__ == '__main__':
                dsAdapter = DSWSAdapter('ZPML029', 'CLICK223')  # Ralph
            
            
                Refin_data = dsAdapter.GetPriceHistForTicker(underlyinglist, fieldList, marketdata_startdate)
            
            #remove multiindex
            #Refin_data.columns=Refin_data.columns.droplevel(1)
            Refin_data.reset_index(inplace=True)
            Refin_data=Refin_data.rename(columns={'Dates': 'Date'})
            Refin_data.set_index('Date',inplace=True)
            
            
            Refin_data.index = pd.to_datetime(Refin_data.index, format='%Y-%m-%d')
            #remove non-priced columns
            Refin_data = Refin_data.dropna(how='all', axis=1)
            
            
            #SAVE BASE REFINITIV DATA TO CSV
            Refin_data.to_csv(base_mdata_file)
            #print(Refin_data)
            
            #INITIATE MARKET DATA CLASS
            market_data = GetMarketData('my_market_data', 1, Refin_data,  0, SABRdata_file, startdate,
                                        Divpoints, gen_tenors, gen_vol_tenor_codes, gen_times, gen_strikes, RFrate_code,
                                        ticker, volticker)
            
            #LOAD IN MARKET DATA
            mdata = market_data.generate_market_data()
            
            #SAVE MARKET DATA DATAFRAME
            mdata.to_csv(output_dir +"MDATA_DF.csv")
            print(mdata)

            #SAVE YIELD CURVE DATAFRAME
            yield_dataframe = market_data.yield_df
            yield_dataframe.to_csv(output_dir + "YIELD_DF.csv")
            #print(yield_dataframe)
            #SAVE SABR VOL DATAFRAME
            SABR_dataframe = market_data.SABR_df
            SABR_dataframe.to_csv(output_dir +"SABR_DF.csv")
            print(SABR_dataframe)
            
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = CreateMarketData()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
