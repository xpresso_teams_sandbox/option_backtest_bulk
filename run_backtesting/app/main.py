"""
This is the implementation of data preparation for sklearn
"""
import pandas as pd
import app.QUANTFUNCTIONSAUG as qnt
import matplotlib.pyplot as plt
import numpy as np

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("run_backtesting",level=logging.INFO)


class My2DScatter():
    def __init__(self):
        pass
    def Scatter2D(self, xseries, yseries , sizesseries,  labels, xaxislabel, yaxislabel, title, savepath):
        plt.style.use('dark_background')
        fig = plt.figure(figsize=(10, 7))
        ax = fig.add_subplot(111)
       
        x = xseries
        y = yseries    
        sizes = sizesseries
        
        colors = list(range(1, len(x)+1))
        colors = ['C' + str(item) for item in colors]

        for i in range(0,len(xseries)):
                ax.scatter(x[i], y[i], c=colors[i], s=sizes[i], marker='o', alpha=0.9, edgecolors='none',label=labels[i])
                plt.text(x[i]+.001, y[i]+.001, labels[i], fontsize=7)
        ax.set_xlabel(xaxislabel)
        ax.set_ylabel(yaxislabel)
        ax.set_title(title)


        lgd = ax.legend(loc='upper left', bbox_to_anchor= (1, 1),
            borderaxespad=0, frameon=True, fontsize=8, ncol=2)
        plt.tight_layout()
        plt.savefig(savepath, bbox_extra_artists=(lgd,), bbox_inches='tight')
        #plt.show()



class My3DScatter():
    def __init__(self):
        pass
    def Scatter3D(self, xseries, yseries , zseries, labels, xaxislabel, yaxislabel, zaxislabel, title, savepath):
        
        with plt.style.context('classic'):
            fig = plt.figure(figsize=(10, 7))
            #ax = fig.add_subplot(111, projection='3d')
            ax = plt.axes(projection ="3d") 
            
            ax.grid(b = True, color ='blue', linestyle ='-.', linewidth = 0.3, alpha = 0.2) 
            
            # Creating color map
            my_cmap = plt.get_cmap('Reds')
           
            x =xseries
            y =yseries
            z =zseries       
              
            colors = list(range(1, len(x)+1))
            colors = ['C' + str(item) for item in colors]
            
            chart = ax.scatter3D(x, y, z, c=(z), cmap=my_cmap, s=100, marker='o', alpha=0.9)
            
            ax.set_xlabel(xaxislabel)
            ax.set_ylabel(yaxislabel)
            ax.set_zlabel(zaxislabel)
            ax.set_title(title)
            fig.colorbar(chart, ax = ax, shrink = 0.5, aspect = 5)
            ax.mouse_init()
            #ax.legend(loc='upper left', bbox_to_anchor= (1, 1),
             #   borderaxespad=0, frameon=True, fontsize=8)
            plt.savefig(savepath)
            #plt.show()
            
            

class DefineSynthDelta1():
    def __init__(self, name, weight, strikepct, callputtype, tenorbdays, tranchebdayfrequency, closeearlybdays, trading_date_list):
        self.name=name
        self.weight=weight
        self.strikepct=strikepct
        self.callputtype=callputtype
        self.tenorbdays=tenorbdays
        self.tranchebdayfrequency=tranchebdayfrequency
        self.closeearlybdays=closeearlybdays
        self.trading_date_list=trading_date_list
        
    #CALCULATE DATE DICTIONARY FOR Delta1
    def Delta1leg_date_dict(self):       
        date_dict={}
        date_dict = qnt.CalcTrancheDates(self.trading_date_list, self.tenorbdays, self.tranchebdayfrequency)
        return date_dict


class DefineLeg():
    def __init__(self, name, weight, strikepct, callputtype, tenorbdays, tranchebdayfrequency, closeearlybdays):
        self.name=name
        self.weight=weight
        self.strikepct=strikepct
        self.callputtype=callputtype
        self.tenorbdays=tenorbdays
        self.tranchebdayfrequency=tranchebdayfrequency
        self.closeearlybdays=closeearlybdays
        
    #CALCULATE DATE DICTIONARY FOR LEG
    def leg_date_dict(self, trading_date_list):       
        date_dict={}
        date_dict = qnt.CalcTrancheDates(trading_date_list, self.tenorbdays, self.tranchebdayfrequency)
        return date_dict
        



class DefineStrategy():
    def __init__(self, name, category, savename, underlyerweight, no_of_legs, maxtenormonths):
        self.name=name
        self.category=category
        self.savename=savename
        self.underlyerweight=underlyerweight
        self.no_of_legs=no_of_legs
        self.maxtenormonths=maxtenormonths




class RunBackTest():
    def __init__(self):
        self.statistics_means_df=pd.DataFrame()
        self.delta1_statistics=pd.DataFrame()
        self.strat_cols=[]
        self.strategy_characteristics_df = pd.DataFrame()
            
    #RUN BACKTEST
    def generate_backtest(self, datafile, stratrow, mdata, yield_df, SABR_df, gen_times, outputsavepath):       
        
        #SET START SPOT TO FIRST ROW SPOTPRICE - THE MDATA_DF frame has already been filtered.
        self.starting_spot = mdata['SpotPrice'][0]
        
        
        row = stratrow
        
        #create new DF for calcs
        calc_df = pd.DataFrame()
        calc_df = mdata.copy()
        
        #CREATE A STRATEGY INSTANCE        
        self.Strategy1 = DefineStrategy(datafile['labelfile'][row], datafile['Categ'][row], datafile['Filename'][row], datafile['underweight'][row], datafile['Legs'][row], datafile['MaxTenorMonths'][row])
        
        #CREATE LEG CLASS INSTANCES
        leg_object_list=[]
        for l in range(1, self.Strategy1.no_of_legs+1):
            leg = DefineLeg( 'leg'+str(l), datafile['W'+str(l)][row], datafile['K'+str(l)][row], datafile['T'+str(l)][row], datafile['days'+str(l)][row], datafile['tranchedays'+str(l)][row] ,datafile['closeearlydays'+str(l)][row])
            leg_object_list.append(leg)
               
        #CREATE DELTA1 CLASS INSTANCE
        # 3M ATM SYNTHETIC 
        synthdelta = DefineSynthDelta1( 'Delta1', 1 , 100, 'S', 60, 60, 0, calc_df)       
        
        #CALCULATE MTM SERIES FOR EACH TRANCHE AND ADD TO DF======================    
        for i in range(0, self.Strategy1.no_of_legs):         
            calc_df = qnt.CalcMTMSeries(calc_df, leg_object_list[i], leg_object_list[i].leg_date_dict(calc_df) , yield_df, gen_times, SABR_df, gen_times, 1)  #dict_list[i]
          
            
        #CALCULATE MTM SERIES FOR DELTA1 AND ADD TO DF   
        calc_df = qnt.CalcMTMSeries(calc_df, synthdelta, synthdelta.Delta1leg_date_dict(), yield_df, gen_times, SABR_df, gen_times,1)
    
        self.calc_backtest_df = calc_df
    
        return calc_df
    
    
  
    def generate_backtest_results(self, datafile, row, outputsavepath):
        # AGGREGATE LEG MTM TOTALS IN NEW RESULTS DF
        
        results_df=pd.DataFrame()
        
        results_df = self.calc_backtest_df.loc[ :, ['SpotPrice','Total_MTM_Delta1']].copy() 
        
        for i in range(1, self.Strategy1.no_of_legs + 1):
            results_df['Total_MTM_leg'+str(i)]=self.calc_backtest_df['Total_MTM_leg'+str(i)]
            results_df['InitialVolleg'+str(i)]=self.calc_backtest_df['InitialVolleg'+str(i)]
            results_df['InitialFVleg'+str(i)]=self.calc_backtest_df['InitialFVleg'+str(i)]
            results_df['FinalFVleg'+str(i)]=self.calc_backtest_df['FinalFVleg'+str(i)]
            results_df['TranchePremiumleg'+str(i)]=self.calc_backtest_df['TranchePremiumleg'+str(i)]
                                
        # CALCULATE STRATEGY MTM COLUMNS    
        results_df['Overlay_Strategy_MTM']=0
        results_df['Strategy_MTM']= 0
        results_df['Strategy_InitialFV']= 0
        results_df['Strategy_FinalFV']= 0
        results_df['Strategy_TranchePremium']= 0
        
        for i in range(1, self.Strategy1.no_of_legs + 1):
            results_df['Overlay_Strategy_MTM'] = results_df['Overlay_Strategy_MTM'] + results_df['Total_MTM_leg'+str(i)]
            results_df['Strategy_MTM'] = results_df['Strategy_MTM'] + results_df['Total_MTM_leg'+str(i)]
            results_df['Strategy_InitialFV'] = results_df['Strategy_InitialFV'] + results_df['InitialFVleg'+str(i)]
            results_df['Strategy_FinalFV'] = results_df['Strategy_FinalFV'] + results_df['FinalFVleg'+str(i)]
            results_df['Strategy_TranchePremium'] = results_df['Strategy_TranchePremium'] + results_df['TranchePremiumleg'+str(i)]
        
        if self.Strategy1.underlyerweight != 0:
            results_df['Strategy_MTM'] = results_df['Strategy_MTM']  + self.Strategy1.underlyerweight * results_df['Total_MTM_Delta1']
                        
        # CALCULATE NORMALISED MTM COLUMNS
        results_df['Overlay_Strategy_Norm_MTM'] = (results_df['Overlay_Strategy_MTM'] + self.starting_spot) / self.starting_spot
        results_df['Strategy_Norm_MTM'] = (results_df['Strategy_MTM'] + self.starting_spot) / self.starting_spot
        results_df['Delta1_Norm_MTM'] = (results_df['Total_MTM_Delta1'] + self.starting_spot) / self.starting_spot
        
        fig,ax = plt.subplots()
        ax.plot(results_df[ ['Strategy_Norm_MTM','Delta1_Norm_MTM'] ])
        ax2=ax.twinx()
        ax2.plot(results_df[ 'Overlay_Strategy_MTM'])
        plt.title('Results '+self.Strategy1.name)
        plt.show()
        
        #results_df.to_csv(outputsavepath + datafile['Filename'][row]+"Results.csv")
        
        self.calc_results_df =  results_df   
        
        return results_df
        

    #RUN STATS
    def generate_backtest_stats(self,   datafile, stratrow):  
        row = stratrow
        run_name = datafile['Strategy'][row]
        
        # CALCULATE SERIES STATISTICS ========
        
        #CALC STATS FOR OPTION STRUCTURE       
        strategy_series_for_stats = pd.DataFrame()
        strategy_series_for_stats['Strategy_Norm_MTM'] = self.calc_results_df['Strategy_Norm_MTM']   
        strategy_series_for_stats.plot()
        plt.title('Result '+run_name)
        plt.show() 
        
        strategy_statistics = qnt.calcseriesstats(strategy_series_for_stats)
        
        #CALC STATS FOR DELTA1 STRUCTURE 
        delta1_series_for_stats = pd.DataFrame()
        plt.title('Result Delta1')
        delta1_series_for_stats['Delta1_Norm_MTM'] = self.calc_results_df['Delta1_Norm_MTM']  
        delta1_series_for_stats.plot()
        plt.show() 
        
        self.delta1_statistics = qnt.calcseriesstats(delta1_series_for_stats)
        
        #CALCULATE STATS MEANS AND ADD TO DF
        strategy_statistics_mean = strategy_statistics.mean()
        
        frames=[self.statistics_means_df, strategy_statistics_mean]
        self.statistics_means_df = pd.concat(frames, axis=1)
         
        self.strat_cols=[]
        for i in range(0, len(self.statistics_means_df.columns)):
           self.strat_cols.append(datafile['labelfile'][i])
                  
        self.statistics_means_df.columns = self.strat_cols

        return self.statistics_means_df      


    def generate_stats_scatter(self, save_path):

        delta1_statistics_mean = self.delta1_statistics.mean()
        frames=[self.statistics_means_df, delta1_statistics_mean]
        self.statistics_means_df = pd.concat(frames, axis=1)
        self.strat_cols.append('Delta1')
        self.statistics_means_df.columns = self.strat_cols     
        self.statistics_means_df=self.statistics_means_df.transpose()
            
        plot_df = self.statistics_means_df[['Return', 'AnnVol', 'MaxDrawdown','Range']]
        
        labels = list(plot_df.index)

        testscatter = My3DScatter()
        testscatter.Scatter3D(plot_df['AnnVol'], plot_df['Return'], plot_df['MaxDrawdown']*-10000, labels, 'Vol','Return', 'Drawdown', 'Scatter3D', save_path+"Scatter3D.png")

        #testscatter = My2DScatter()
        #testscatter.Scatter2D(plot_df['AnnVol'], plot_df['Return'], plot_df['MaxDrawdown']*-1000, labels, 'Vol','Return', 'Scatter2D - Size is MaxDrawdown', save_path+"Scatter2D.png")
     





    def generate_strat_series_characteristics(self, save_path):
        
        ####### NEED TO DO EVERYTHING AS TICK CHANGES OVER INITIAL SPOT
        ##OTHERISE YOU CAN GO DOWN LOTS FIRST, THEN RALLY BIG RETURNS, BUT STILL END UP < START VALUE
        # IS THIS RIGHT OR SHOULD WE JUST LOOK AT SERIES LOG RETURNS???

        self.strategy_cost_benefit_df = self.calc_results_df.loc[:, ['SpotPrice','Strategy_InitialFV','Strategy_FinalFV','Overlay_Strategy_MTM' , 'Strategy_Norm_MTM', 'Delta1_Norm_MTM'] ]
        
        self.strategy_cost_benefit_df['SpotMove'] = 0
        self.strategy_cost_benefit_df['OverlayMove']=0
        
        #see above re how we calc returns
        #THESE ARE AS %START
        self.strategy_cost_benefit_df['SpotMove'] = (self.calc_results_df['SpotPrice'] - self.calc_results_df['SpotPrice'].shift(1)) / self.starting_spot
        self.strategy_cost_benefit_df['OverlayMove'] = (self.calc_results_df['Overlay_Strategy_MTM'] - self.calc_results_df['Overlay_Strategy_MTM'].shift(1)) / self.starting_spot
        
        #set first row where we have nans =0
        self.strategy_cost_benefit_df['SpotMove'][0]=0
        self.strategy_cost_benefit_df['OverlayMove'][0]=0

        plot_df = self.strategy_cost_benefit_df['OverlayMove']
        plt.hist(plot_df, bins=50)
        plt.show()
        
        #CALCULATE STRATEGY CHARACTERISTICS ================================
        
        #print(skew(plot_df))
        no_of_obs = self.strategy_cost_benefit_df['OverlayMove'].count()
        print(no_of_obs)
        
        #convex return = >2%
        conv_return = 0.02
        
        self.strategy_Cost = -1 * (self.strategy_cost_benefit_df.loc[ self.strategy_cost_benefit_df['OverlayMove'] < 0] ['OverlayMove'].mean()
                                * self.strategy_cost_benefit_df.loc[ self.strategy_cost_benefit_df['OverlayMove'] < 0] ['OverlayMove'].count() / no_of_obs)
        self.strategy_Benefit = (self.strategy_cost_benefit_df.loc[(self.strategy_cost_benefit_df['OverlayMove'] >= 0) &  (self.strategy_cost_benefit_df['OverlayMove'] < conv_return)]['OverlayMove'].mean()
                                 *self.strategy_cost_benefit_df.loc[(self.strategy_cost_benefit_df['OverlayMove'] >= 0) &  (self.strategy_cost_benefit_df['OverlayMove'] < conv_return)]['OverlayMove'].count() / no_of_obs)
        self.strategy_Convexity = (self.strategy_cost_benefit_df.loc[self.strategy_cost_benefit_df['OverlayMove'] > conv_return]['OverlayMove'].mean()
                                      *self.strategy_cost_benefit_df.loc[self.strategy_cost_benefit_df['OverlayMove'] > conv_return]['OverlayMove'].count() / no_of_obs)
        
        #PREVENT ERRORS IF NO CONVEXITY OBSERVATIONS
        
        self.strategy_Convexity = [self.strategy_Convexity if self.strategy_Convexity>0 else 0][0]
        
        self.strategy_BenefitConvCost_ratio = (self.strategy_Benefit+self.strategy_Convexity)/self.strategy_Cost
        
        
        #CALC MTM CORRELATION WITH Delta1
        self.strategy_Correlation = self.calc_results_df['Strategy_Norm_MTM'].corr( self.calc_results_df['Delta1_Norm_MTM'])
        
        #CALC MTM DOWN CORRELATION WITH DELTA1
        # should we use 1day, 5 day, 1month correlation?
        down_corr_df = self.strategy_cost_benefit_df.loc[self.strategy_cost_benefit_df['SpotMove'] <0]
        
        self.strategy_down_correlation = [-1 * down_corr_df['OverlayMove'].corr( down_corr_df['SpotMove']) if  down_corr_df['OverlayMove'].corr( down_corr_df['SpotMove']) <0 else 0][0]
        
        print(self.strategy_down_correlation)
        
        
        #ADD IN SPEED OF DECAY BIT HERE
        #USE UP CORRELATION? Over what time frame?
        up_corr_df = self.strategy_cost_benefit_df.loc[self.strategy_cost_benefit_df['SpotMove'] >0]
        
        self.strategy_up_correlation = [-1 * up_corr_df['OverlayMove'].corr( up_corr_df['SpotMove']) if  up_corr_df['OverlayMove'].corr( up_corr_df['SpotMove']) <0 else 0][0]
        
        
        
        #ADD TO DF
        
        character_values_list = [self.strategy_Cost, self.strategy_Benefit, self.strategy_BenefitConvCost_ratio, self.strategy_Convexity, self.strategy_down_correlation, self.strategy_up_correlation ]

        index_list = ['Cost', 'Benefit', 'Ben+Conv-CostRatio', 'Convexity','Down-Corr', 'Decay']
        
        self.strategy_characteristics_df[self.Strategy1.name] = character_values_list
        self.strategy_characteristics_df.index = index_list 
        self.strategy_characteristics_df = self.strategy_characteristics_df.replace(np.nan, 0)
        
        
        return self.strategy_characteristics_df
    
        

class RunBacktesting(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="RunBacktesting")
        """ Initialize all the required constansts and data her """

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            
            gen_tenors=["1M","3M","1Y"]
            gen_vol_tenor_codes = ['O1', 'O3', 'OY']
            gen_times=[0.084,0.25,1]
            gen_strikes=[80, 90, 95, 100, 105, 110, 115]
            
            
            #OUTPUT DIRECTORY FOR COMPONENT3       
            output_dir="/data/"
            #DIRECTORY FOR INPUT FILES
            input_dir="/data/"
            
            #LOAD STRATEGIES FILE
            datafile = pd.read_csv(input_dir + "StrategyDataFile.csv")
            
            
            #LOAD IN MARKET DATA FILES
            mdata = pd.read_csv(input_dir + "MDATA_DF.csv", index_col=[0])
            yield_df = pd.read_csv(input_dir + "YIELD_DF.csv", index_col=[0])
            SABR_df = pd.read_csv(input_dir + "SABR_DF.csv", index_col=[0])
            #SET INDEX TO DATETIME
            mdata.index = pd.to_datetime(mdata.index, format='%Y-%m-%d')
            yield_df.index = pd.to_datetime(yield_df.index, format='%Y-%m-%d')
            SABR_df.index = pd.to_datetime(SABR_df.index, format='%Y-%m-%d')
            
            
            #RUN BACKTESTS 
            #INITIATE BACKTESTS CLASS
            backtest_run=RunBackTest()
            
            #RUN EACH BACKTEST
            for i in range(0, len(datafile)):
                calculations_df = backtest_run.generate_backtest(datafile, i, mdata, yield_df, SABR_df, gen_times, output_dir)
                results_df =  backtest_run.generate_backtest_results(datafile, i, output_dir)
                stats_df = backtest_run.generate_backtest_stats(datafile, i)
                strat_characters_df = backtest_run.generate_strat_series_characteristics(output_dir)
                print(i)
                print(stats_df.transpose())
                       
            #SHOW SCATTER CHARTS
            backtest_run.generate_stats_scatter(output_dir)
            
            
            #SAVE STATS DATAFRAME
            backtest_run.statistics_means_df.to_csv(output_dir + "Statistics.csv")
            
            #SAVE STRATS CHARACTERISTICS DATAFRAME
            strat_characters_df=strat_characters_df.transpose()
            strat_characters_df.to_csv(output_dir + "StratCharacteristics.csv")
            
                        
                        
            
            
            
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = RunBacktesting()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
