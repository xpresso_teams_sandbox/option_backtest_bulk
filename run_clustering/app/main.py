"""
This is the implementation of data preparation for sklearn
"""
import pandas as pd
import matplotlib.pyplot as plt
from math import pi
#CLUSTERING
from sklearn.preprocessing import normalize
from sklearn import preprocessing
import scipy.cluster.hierarchy as shc
from sklearn.cluster import AgglomerativeClustering

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("run_clustering",level=logging.INFO)



class Clustering():
    def __init__(self):
        pass
    
    def clusters_normalisedata(self, df, savepath):

        self.strategy_characteristics_df = df.copy()
        
        mm_scaler = preprocessing.MinMaxScaler()
        
        cluster_by_category = 1
        
        self.data = self.strategy_characteristics_df#.transpose().copy()
        index_list = self.data.index.to_list()
        
        if cluster_by_category==1:
            
            mmnorm_df = mm_scaler.fit_transform(self.data)
                   
            self.data_scaled = pd.DataFrame(mmnorm_df, columns = self.data.columns)
           
        else:   
            self.data_scaled = normalize(self.data)
    
            self.data_scaled = pd.DataFrame(self.data_scaled, columns = self.data.columns)
            
            
        self.data_scaled['desc'] = index_list
        
        self.data_scaled.set_index('desc', inplace=True)
        
        print(self.data_scaled)
    
        self.clusters_normdf = self.data_scaled
        
        #plot Dendrogram
        plt.figure(figsize=(10,7))
        plt.title('Dendrogram')
        dend = shc.dendrogram(shc.linkage(self.data_scaled, method='ward'), labels=self.data_scaled.index.to_list(), leaf_rotation=90, leaf_font_size=8)
        plt.axhline(y=0.3, color='r', linestyle='--')
        plt.savefig(savepath)
        return self.clusters_normdf
    
    
    
    def clusters_fitdata(self, clusters_n):

        self.cluster = AgglomerativeClustering(n_clusters=clusters_n, affinity = 'euclidean', linkage='ward')
        
        self.cluster.fit_predict(self.clusters_normdf)
        
        plt.figure(figsize=(10,7))
        plt.scatter(self.clusters_normdf['Cost'], self.clusters_normdf['Down-Corr'], c=self.cluster.labels_)
        plt.xlabel('Cost')
        plt.ylabel('Down-Corr')
        #ADD LABELS TO NORM_DF
        self.clusters_normdf['Cluster']=self.cluster.labels_
    
        return self.clusters_normdf    
    


def plot_radar(df, savepath):
    # categories
    categories = df.columns.tolist()
    N = len(categories) # get number of categories
    
    fig = plt.figure(figsize=(12,8))
    ax = plt.subplot(111, polar="True")
    
    # calculate angle for each category
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1] # repeat first angle to close poly
    
    # values
    for i in range(0, len(df)-1):
        values=[]
        values= df.iloc[i, : ].values.tolist()
        values += values[:1] # repeat first value to close poly
        # plot
        plt.polar(angles, values, marker='.') # lines
        #plt.fill(angles, values, alpha=0.3) # area
    
    # xticks
    plt.xticks(angles[:-1], categories)
    # yticks
    ax.set_rlabel_position(0) # yticks position
    plt.yticks([0, 1], color="grey", size=10)
    plt.ylim(0,1)
    plt.savefig(savepath)
    plt.show()



class RunClustering(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="RunClustering")
        """ Initialize all the required constansts and data her """

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            
            
            
            #OUTPUT DIRECTORY FOR COMPONENT3       
            output_dir="/data/"
            #DIRECTORY FOR INPUT FILES
            input_dir="/data/"
            
            
            
            #START CLUSTERING======================================================
                    
            #LOAD THE STRATEGY CHARACTERISTICS DF FROM COMPONENT 3
            strat_characters_df = pd.read_csv(input_dir + "StratCharacteristics.csv",index_col=[0])        
                
            
            #INITIALISE CLUSTERING CLASS      
            cluster_run = Clustering()    
                
            #RUN DATA NORMALISING
            Clustering_norm_df = cluster_run.clusters_normalisedata(strat_characters_df, output_dir+"Dendrogram.png")
            
            cluster_number=int(sys.argv[2])
            
            Clustering_fit = cluster_run.clusters_fitdata(cluster_number)
            
            print(Clustering_fit)
            
            
            #SHOW UNCLUSTERED SPIDER CHARTS
            plot_radar(strat_characters_df, output_dir+'UnnormSpider.png')
            plot_radar(Clustering_fit.iloc[:, 0:6], output_dir+'NormSpider.png')
            
            
            #SPIDER PLOT OF EACH CLUSTER
            for i in range(0,cluster_number):
                plot_radar(Clustering_fit.loc[Clustering_fit['Cluster']==i].iloc[:, 0:6], output_dir+'Cluster'+str(i)+'.png')
            #plot_radar(Clustering_fit.loc[Clustering_fit['Cluster']==1].iloc[:, 0:6], output_dir+'Cluster1.png')
            #plot_radar(Clustering_fit.loc[Clustering_fit['Cluster']==2].iloc[:, 0:6], output_dir+'Cluster2.png')
            #plot_radar(Clustering_fit.loc[Clustering_fit['Cluster']==3].iloc[:, 0:6], output_dir+'Cluster3.png')
            
            #SAVE CLUSTER DATAFRAME
            Clustering_fit.to_csv(output_dir + "Clustering_fit.csv")
                        
                        
            
            
            
            
            
            
            
            
            
            
            
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = RunClustering()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
