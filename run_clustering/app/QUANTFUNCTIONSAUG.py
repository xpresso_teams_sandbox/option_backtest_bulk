# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 14:27:34 2020

@author: ralphm
"""
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 28 13:34:38 2020

"""
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from datetime import date, timedelta
import numpy as np
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from scipy import stats
import scipy as sc
import math
from scipy.optimize import minimize
#from xbbg import blp
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay
us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())


#========================================XBBG BLOOMBERG DATA CALLS========================================================================
'''
#Returns a Single variable output from XBBG result dataframe 
def bpdsingle(bbgcode, bbgfield):

    try:
        bbgask = blp.bdp(tickers=bbgcode, flds=bbgfield)
        output=bbgask
        output=output.iloc[0,0]
    except:
        bbgask=np.nan
        output=bbgask
    return output


#returns ordered dataframe for a list of codes and fields
def bpdcall2(bbgcode, bbgfield):
    field_bbg=bbgfield
    bbgcodelist=list(bbgcode)
    try:
        bbgask = blp.bdp(tickers=bbgcodelist, flds=field_bbg)
        bbgask=bbgask.reset_index()
        indexholder=[]
        for item in bbgask['index']:
            indexholder.append(bbgcodelist.index(item))
        bbgask['bbgreorder']=indexholder
        bbgask=bbgask.sort_values("bbgreorder")
        bbgask.set_index('bbgreorder',inplace=True)
    except:
        bbgask=np.nan
    return bbgask


#returns a dataframe of tenors and deltavols with deltastrike for a given index - GENERIC TENORS
#NOTE YOU JUST CALL WITH SX5E, NOT SX5E Index
def bpddeltadf(bbgcode):

    tenors=["1M","2M","3M","6M","9M","1Y","18M","2Y","3Y","4Y"]
    times=[0.084,0.167,0.252,0.498,0.75,1,1.5,2,3,4]
    desc=["1m","2m","3m","6m","9m","1y","18m","2y","3y","4y"]
    deltas=["5DP", "10DP","25DP","50D","25DC","10DC"]
    cols=["5DP","10DP","25DP","50D","25DC","10DC","strike5dp","strike10dp","strike25dp","strike50d","strike25dc","strike10dc"]
    RFdict={"USD":"S0023Z",
            "EUR":"S0045Z",
            "GBP":"S0022Z",
            "JPY":"S0013Z",
            "HKD":"S0010Z"}
    bbgcodelist=[]   
    for i in range(0,len(tenors)):
        for j in range(0, len(deltas)):
            bbgcodelist.append(bbgcode+" "+tenors[i]+" "+deltas[j]+" VOL BVOL Index")
    try:
        bbgask = blp.bdp(tickers=bbgcodelist, flds=["PX_LAST","OPT_STRIKE_PX"])
        bbgask=bbgask.reset_index()
        indexholder=[]
        for item in bbgask['index']:
            indexholder.append(bbgcodelist.index(item))
        bbgask['bbgreorder']=indexholder
        bbgask=bbgask.sort_values("bbgreorder")
        bbgask.set_index('bbgreorder',inplace=True)
    except:
        bbgask=np.nan
    formateddf=pd.DataFrame()
    for n in range(0,len(tenors)):
        row1=len(deltas)*n
        row2=len(deltas)*(n+1)
        tempdf=bbgask.iloc[row1:row2,0:2].copy()
        tempstrike=bbgask.iloc[row1:row2,[0,2]].copy()
        tempdf.set_index("index", inplace=True)
        tempstrike.set_index("index", inplace=True)
        tempdf=tempdf.transpose()
        tempdf.reset_index(inplace=True)
        tempstrike=tempstrike.transpose()
        tempstrike.reset_index(inplace=True)
        horizontal_stack = pd.concat([tempdf, tempstrike], axis=1)
        horizontal_stack = horizontal_stack.drop(horizontal_stack.columns[[0,7]], axis=1)
        horizontal_stack.columns=cols
        frames=[formateddf,horizontal_stack]
        formateddf=pd.concat(frames)
    formateddf['Tyrs']=times
    formateddf['Desc']=desc
    formateddf['PX_LAST']=blp.bdp(tickers=bbgcode+" Index", flds="PX_LAST").iloc[0,0]
    currency=blp.bdp(tickers=bbgcode+" Index", flds="CRNCY").iloc[0,0]
    ratecode=RFdict[currency]
    RFlist=[]
    for k in range(0,len(tenors)):
        RFlist.append(blp.bdp(tickers=ratecode+" "+tenors[k]+ " BLC2 Curncy", flds="PX_LAST").iloc[0,0])
    formateddf['RF']=RFlist
    formateddf.reset_index(inplace=True)
    formateddf = formateddf.drop(formateddf.columns[[0]], axis=1)
    return formateddf


#returns a strike ordered and filtered dataframe of OptionPrices, Implied Vols, rates et for a single INDEX Expiry - SINGLE FIXED TENOR
#NOTE YOU JUST CALL WITH SX5E, NOT SX5E Index
def bpdsingleindexexpirydf(Underlyer, Expiry, absKmin, absKmax, Kinterval):

    rawdata=pd.DataFrame()
    bbgfields=["IVOL_BID","IVOL_ASK","IVOL_MID","OPT_UNDL_PX","OPT_FINANCE_RT","OPT_DIV_YIELD","IMPLIED_FORWARD_PRICE", "PX_BID", "PX_ASK"]
    todaysdate = date.today()
    expirydate=datetime.datetime.strptime(Expiry, '%m/%d/%y').date()
    strikeperiods = int(( absKmax - absKmin ) / Kinterval)
    tickerlist=[]
    expirylist=[]
    strikelist=[]
    Typelist=[]
    Timelist=[]
    for i in range(0,strikeperiods):
        tickerlist.append(Underlyer+" "+Expiry + " " + "C"+ str(absKmin + i*Kinterval) + " Index")
        expirylist.append(Expiry)
        strikelist.append(absKmin + i*Kinterval)
        Typelist.append("Call")
        Timelist.append(max(((expirydate -  todaysdate).days / 365), 1 / 365))
    for i in range(0,strikeperiods):
        tickerlist.append(Underlyer+" "+Expiry + " " + "P"+ str(absKmin + i*Kinterval) + " Index")
        expirylist.append(Expiry)
        strikelist.append(absKmin + i*Kinterval)
        Typelist.append("Put")
        Timelist.append(max(((expirydate - todaysdate).days / 365), 1 / 365))
    rawdata = pd.DataFrame(list(zip(tickerlist,expirylist)), columns = ['OPT_CHAIN', 'Expiry'])
    rawdata['Strike']=strikelist
    rawdata['Type']=Typelist
    rawdatabbg=bpdcall2(rawdata['OPT_CHAIN'],bbgfields )
    rawdatabbg=rawdatabbg.rename(columns={'index': 'OPT_CHAIN'})
    rawdata=pd.merge(rawdata,rawdatabbg, on='OPT_CHAIN',how='inner' )

    collist=['OPT_CHAIN','Expiry','Strike','Type','IVOL BID','IVOL ASK','VoltoFit','Spot','RFrate','ImpDivyield','Forward', 'Bid','Ask' ]
    rawdata.columns=collist
    rawdata['Time'] = Timelist
    rawdata['Call Bid']=rawdata.apply(lambda x: x['Bid'] if x['Type']=='Call' else 0, axis=1)
    rawdata['Call Ask'] = rawdata.apply(lambda x: x['Ask'] if x['Type']== 'Call' else 0, axis=1)
    rawdata['Put Bid']=rawdata.apply(lambda x: x['Bid'] if x['Type']=='Put' else 0, axis=1)
    rawdata['Put Ask'] = rawdata.apply(lambda x: x['Ask'] if x['Type']== 'Put' else 0, axis=1)
    rawdata['Bid Ivol']=rawdata.apply(lambda x: x['IVOL BID']/100 , axis=1)
    rawdata['Ask Ivol'] = rawdata.apply(lambda x: x['IVOL ASK']/100 , axis=1)
    rawdata["MktVol"] = rawdata["VoltoFit"].apply(lambda x: x / 100)
    #filter to use calls above fwd, puts below fwd
    def removevols(type, fwd, strike):
        if type=="Call":
            if strike<=fwd:
                return 0
            else:
                return 1
        else:
            if strike<fwd:
                return 1
            else:
                return 0

    rawdata["Volfactor"]=rawdata.apply(lambda x: removevols(x["Type"],x["Forward"],x["Strike"]),axis=1)
    #set to zero any strikes for ITM calls and Puts
    rawdata.MktVol=rawdata.MktVol*rawdata.Volfactor
    rawdata=rawdata[rawdata.MktVol>0.01].copy(deep=True)
    #sort by strike
    rawdata.sort_values(by=["Strike"],inplace=True)
    rawdata=rawdata.reset_index(drop=True)

    return rawdata
'''

#===================================== OPTION BACKTESTING FUNCTION ==================================================================


def CalcMTMSeries(df, legclassobject, dates_dict_leg, yield_df, yield_tenor_list, SABR_df, SABR_tenorsyrs, isundiscounted):
        
    if legclassobject.callputtype=='C':
        optiontype = 1
    elif legclassobject.callputtype=='P':
        optiontype = -1
    else:
        optiontype = 0  #Synthetics
    
    initial_vol_list=[]
    initial_fv_list=[]
    final_fv_list=[]
    
    #run for each tranche
    for i in range(0, len(list(dates_dict_leg.keys()))):
        
        colheader = legclassobject.name +'MTM '+ str(list(dates_dict_leg.keys())[i])
        
        tranche_mtm_series=[]
        
        strikedate = list(dates_dict_leg.keys())[i]
        expirydate = dates_dict_leg[strikedate]
        initial_spot = df['SpotPrice'][strikedate]
        initial_strike = initial_spot * legclassobject.strikepct/100
        initial_divyield =  df['DivYield'][strikedate]
        initial_TTE = (expirydate-strikedate).days/365
        initial_yield = yieldcurveinterp(yield_df, yield_tenor_list, initial_TTE, strikedate )/100
        initial_fwd = fwdpricecalc(initial_spot,initial_yield, initial_divyield, initial_TTE)
        initial_sabr_params =  SABRcurveinterp(SABR_df, SABR_tenorsyrs, initial_TTE, strikedate)     
        initial_vol= SABR(initial_sabr_params[0], initial_sabr_params[1], initial_sabr_params[2], initial_sabr_params[3], initial_fwd, initial_strike, initial_TTE, 0)
        initial_vol_list.append(initial_vol)
        
        initial_premium = black_scholes(initial_spot, initial_strike, initial_TTE, initial_vol, initial_yield, initial_divyield , optiontype, 'P', isundiscounted)        

        initial_fv_list.append(legclassobject.weight * initial_premium) #work out premium spend (this is to track relative costs in different environments)

        #set close date if closed early
        if legclassobject.closeearlybdays == 0:
            close_date = expirydate
        elif legclassobject.closeearlybdays > 0:
            
            try: # this handles if close date > last data point
                close_indexer = df.index.get_loc(expirydate) - legclassobject.closeearlybdays
                close_date = df.index[close_indexer] 
            except:
                close_date=expirydate
        

        #run through every day
        for j in range(0, len(df)):
                        
            if df.index[j] <strikedate: #If before strikedate, MTM=0
                tranche_mtm_series.append(0)
                
            elif df.index[j] >=strikedate: #if after strikedate
                if df.index[j] <= close_date: #and less than or = closedate
                    #Now work out the MTM P&L series
                    current_spot = df['SpotPrice'][j]
                    current_TTE = max((expirydate-df.index[j]).days/365, 0.000001)
                    current_yield = yieldcurveinterp(yield_df, yield_tenor_list, current_TTE, df.index[j] )/100
                    current_divyield =  df['DivYield'][j]
                    current_fwd = fwdpricecalc(current_spot,current_yield, current_divyield, current_TTE)     
                    current_sabr_params =  SABRcurveinterp(SABR_df, SABR_tenorsyrs, current_TTE, df.index[j])  
                    current_vol = SABR(current_sabr_params[0], current_sabr_params[1], current_sabr_params[2], current_sabr_params[3], current_fwd, initial_strike, current_TTE, 0)  
        
                    current_premium = black_scholes(current_spot, initial_strike, current_TTE, current_vol, current_yield, current_divyield , optiontype, 'P', isundiscounted)
                    
                    current_mtm = legclassobject.weight * (current_premium-initial_premium)
                    tranche_mtm_series.append(current_mtm)                       
                                
                elif df.index[j] > close_date: #if after closedate set to close date value
                    tranche_mtm_series.append(tranche_mtm_series[-1])
        
        final_fv_list.append(tranche_mtm_series[-1])
    
        df[colheader]=tranche_mtm_series

    #Calc MTM total - weight this for the number of tranches (and take account of early close)
    legmtm_cols = [col for col in df.columns if legclassobject.name +'MTM ' in col]
    df['Total_MTM_'+legclassobject.name]= df.loc[ :, legmtm_cols ].sum(axis=1) * (legclassobject.tranchebdayfrequency/(legclassobject.tenorbdays - legclassobject.closeearlybdays))


    #CREATE DF AND PLOT CHARTS FOR VOL AND PREMIUM TO CHECK DATA
    initial_vol_df=pd.DataFrame(list(dates_dict_leg.items()),columns = ['Start','Expiry'])
    initial_vol_df['InitialVol'+legclassobject.name]= initial_vol_list
    initial_vol_df['InitialFV'+legclassobject.name]=initial_fv_list
    initial_vol_df['FinalFV'+legclassobject.name]=final_fv_list
    initial_vol_df['TranchePremium'+legclassobject.name] = initial_vol_df['InitialFV'+legclassobject.name] * (legclassobject.tranchebdayfrequency/(legclassobject.tenorbdays - legclassobject.closeearlybdays))

    
    initial_vol_df.set_index('Start', inplace=True, drop=True)
    del initial_vol_df['Expiry']
    
    initial_vol_df['InitialVol'+legclassobject.name].plot()
    plt.title('InitialVol'+legclassobject.name)
    plt.show()
    initial_vol_df['InitialFV'+legclassobject.name].plot()
    plt.title('InitialFV'+legclassobject.name)
    plt.show()
    initial_vol_df['TranchePremium'+legclassobject.name].plot()
    plt.title('TranchePremium'+legclassobject.name)
    plt.show()
    
    #Add the initial vol, initial premium and tranche premium to the dataframe
    df=pd.merge(df, initial_vol_df, on=None, left_index=True, right_index=True, how='left') 
    
    return df







#===================================== YIELD CURVE and DIVIDEND FUNCTIONS ==================================================================




#df is dataframe of bbg rf rates, with datetimeindex
#Ratetenorlist is a list of tenors in years, ordered, T is the maturity, date is the date in question
def yieldcurveinterp(df, Ratetenorlist, T, date):
    
    rateseries=df.loc[date]
    
    if T<Ratetenorlist[0]:
        rfrate=rateseries[0]     
    elif T>Ratetenorlist[-1]:
        rfrate=rateseries[-1]   
    else:
        x = Ratetenorlist
        y = rateseries
        f = interp1d(x, y)     
        rfrate=f(T)     
    return rfrate


#df is dataframe of spot prices, with datetimeindex
#divdict is a dictionary of div points per year
def calcdivyield(df, divdict):
    
    df['Year']=df.index.year 
    divyieldseries=[]
    for i in range(0,len(df['Year'])):
        divpoints=divdict.get(str(df['Year'][i]))
        divyield=divpoints/df[i]                                   
        divyieldseries.append(divyield)
    return divyieldseries



def fwdpricecalc(Spot,Rate, Div, T):

    Fwdprice=Spot*np.exp((Rate-Div)*T)
    
    return Fwdprice






#===================================== DATE FUNCTIONS ===================================================================================

def CalcTrancheDates(data, busdayexpiry, tranchebusdayfrequency):
    
    dates_dict = dict()
    
    dates = data.index
    
    startdate_list=[]
    expirydate_list=[]
    
    no_of_tranches=int(len(dates)/int(tranchebusdayfrequency)) + 1
    
    #Calc start date for each tranche
    for i in range(0,no_of_tranches):
        startdate_list.append(dates[i*tranchebusdayfrequency])
        
    for j in range(0,len(startdate_list)):     
        try:
            expirydate_list.append(dates[ j*int(tranchebusdayfrequency) + int(busdayexpiry)  ] )     
        except:
            expirydate_list.append(dates[ j*int(tranchebusdayfrequency)] +timedelta(days=int(busdayexpiry*365/252)) )
    
    for i in range(0,len(startdate_list)):
        dates_dict[startdate_list[i]] = expirydate_list[i] # Dictionary of dates with start date the key and end date the value
   
    return dates_dict



#===================================== TIMESERIES STATS FUNCTIONS ==================================================================

#df is a single timeseries, with datetimeindex
def calcseriesstats(df):

    logreturnsdf = np.log((df.shift(1) / df))
    #print(logreturnsdf)
    
    counts=pd.DataFrame()
    stddevs=pd.DataFrame()
    means=pd.DataFrame()
    maxreturns=pd.DataFrame()
    minreturns=pd.DataFrame()
    returnstart = pd.DataFrame()
    returnend = pd.DataFrame()
    years=df.index.year.unique()
    
    for y in df.index.year.unique():
        frames=[counts, df.loc[df.index.year==y].count() ]
        counts=pd.concat(frames, axis=1)
        frames1 = [stddevs, logreturnsdf.loc[logreturnsdf.index.year == y ].std()]
        stddevs=pd.concat(frames1, axis=1)
        frames2 = [means, df.loc[df.index.year == y].mean()]
        means=pd.concat(frames2, axis=1)
        frames3 = [minreturns, df.loc[df.index.year == y].min()]
        minreturns=pd.concat(frames3, axis=1)
        frames4 = [maxreturns, df.loc[df.index.year == y].max()]
        maxreturns=pd.concat(frames4, axis=1)
        frames5 = [returnstart , df.loc[df.index.year == y].iloc[0]]
        returnstart=pd.concat(frames5, axis=1)
        frames6 = [returnend , df.loc[df.index.year == y].iloc[-1]]
        returnend=pd.concat(frames6, axis=1)


    collist0=[]
    for i in range(0,len(years)):
        collist0.append(0)

    returnstart.columns=collist0
    returnend.columns=collist0

    frames=[counts,stddevs,means,minreturns, maxreturns,returnstart,returnend ]
    statsdf = pd.concat(frames, axis=0)
    statsdf.columns = years
    statsdf['Statistic']=['count','stddev','mean','min', 'max', 'start', 'end']
    statsdf=statsdf.reset_index()
    statsdf.set_index('Statistic', inplace=True)
    statsdf=statsdf.drop(['index'],axis=1)
    statsdf=statsdf.transpose()
    statsdf['Range']=statsdf['max']-statsdf['min']
    statsdf['Return']=statsdf['end']-statsdf['start']
    statsdf['MaxDrawdown']=statsdf['min']-statsdf['start']
    statsdf['AnnVol'] = statsdf['stddev'] * np.sqrt((252))
    statsdf['Sharpe']=statsdf['Return']/statsdf['AnnVol']

    return statsdf


#===================================== OPTION  FITTING FUNCTIONS ==================================================================




#Fits SABR parameters to DF of input vols (as %strike) - returns a dataframe
# have df dataframe with fwds as 1M_fwd etc, you need generic tenors, times (TTE in yrs), and generic strikes we are using
#underlyer is just 'SX5E', and starting guesses
def bulk_sabr_fit(df, gentenors, times, genKlist, underlyer, Salpha,Srho,Snu):

    underlyercode = underlyer+' Index'
    dfoutput=pd.DataFrame()
    
    for y in range(0, len(gentenors)):
        SABR_params_list=[]
        
        for i in range(0,len(df)):
    
            colstring=gentenors[y]+'_fwd'
            fwd = df[colstring][i]
            
            bbgcodelist=[]
            fitting_vollist=[]
            fitting_strikelist=[]
            dfout=pd.DataFrame()
            
            for j in range(0, len(genKlist)):
                
                bbgcodelist.append(underlyer+" "+gentenors[y]+" "+str(genKlist[j])+" VOL BVOL Index")
                
                fitting_vollist.append(df[bbgcodelist[j], 'PX_LAST'] [i]/100 )
                abs_strike=genKlist[j]/100 * df[underlyercode,'PX_LAST'][i]
                fitting_strikelist.append(abs_strike)
                
                df_list=list(zip(fitting_strikelist,fitting_vollist))
                dfout=pd.DataFrame(df_list, columns=['Strike','Vol'])
                
    
            SABR_params=FitSABRparams(dfout, fwd, times[y], 100, 10000, Salpha,Srho,Snu  )
            SABR_params_list.append(SABR_params)
        
        headers= [ gentenors[y]+'_alpha', gentenors[y]+'_beta', gentenors[y]+'_rho' , gentenors[y]+'_vv' ]
        SABR_params_df=pd.DataFrame(SABR_params_list, columns=headers)       
    
        dfoutput = pd.concat( [dfoutput, SABR_params_df], axis=1)
    
    dfoutput['Date'] = df.index
    dfoutput.set_index('Date', inplace = True, drop = True)

    return dfoutput



#Fits SABR parameters to DF of input vols (as %strike) - returns a dataframe
# have df dataframe with fwds as 1M_fwd etc, you need generic tenors, times (TTE in yrs), and generic strikes we are using
#underlyer is just 'SX5E', and starting guesses
def bulk_sabr_fit_refinitiv(df, gentenors, genvoltenorcodes, times, genKlist, volcode, Salpha,Srho,Snu):

    voltenorcode = genvoltenorcodes
    
    dfoutput=pd.DataFrame()
    
    for y in range(0, len(gentenors)):
        SABR_params_list=[]
        
        for i in range(0,len(df)):
    
            colstring=gentenors[y]+'_fwd'
            fwd = df[colstring][i]
            
            bbgcodelist=[]
            fitting_vollist=[]
            fitting_strikelist=[]
            dfout=pd.DataFrame()
            
            for j in range(0, len(genKlist)):
                
                bbgcodelist.append(volcode+str(genKlist[j]))
                
                fitting_vollist.append(df[bbgcodelist[j], voltenorcode[y]] [i]/100 )
                abs_strike=genKlist[j]/100 * df['SpotPrice','PI'][i]
                fitting_strikelist.append(abs_strike)
                
                df_list=list(zip(fitting_strikelist,fitting_vollist))
                dfout=pd.DataFrame(df_list, columns=['Strike','Vol'])
                
    
            SABR_params=FitSABRparams(dfout, fwd, times[y], 100, 10000, Salpha,Srho,Snu  )
            SABR_params_list.append(SABR_params)
        
        headers= [ gentenors[y]+'_alpha', gentenors[y]+'_beta', gentenors[y]+'_rho' , gentenors[y]+'_vv' ]
        SABR_params_df=pd.DataFrame(SABR_params_list, columns=headers)       
    
        dfoutput = pd.concat( [dfoutput, SABR_params_df], axis=1)
    
    dfoutput['Date'] = df.index
    dfoutput.set_index('Date', inplace = True, drop = True)

    return dfoutput





def interpseries(tenorsyrs, series, Tyrs):
    if Tyrs<=tenorsyrs[0]:
        param=series[0]    
    
    elif Tyrs>=tenorsyrs[-1]:
        param=series[-1]   
    else:
        x = tenorsyrs
        y = series
        f = interp1d(x, y)     
        param=f(Tyrs)    

    return param




#df is dataframe of SABR params, with datetimeindex
#tenorsyrs is a list of tenors in years, ordered, T is the maturity, date is the date in question
def SABRcurveinterp(df, tenorsyrs, T, date):
    
    sabr_param_series=df.loc[date]
    
    alpha_list=[]
    beta_list=[]
    rho_list=[]
    vv_list=[]
    
    for i in range(0, int(len(sabr_param_series)/4)):
        alpha_list.append(sabr_param_series[4*i])
        beta_list.append(sabr_param_series[4*i+1])
        rho_list.append(sabr_param_series[4*i+2])
        vv_list.append(sabr_param_series[4*i+3])
    
    alpha=interpseries(tenorsyrs, alpha_list,T)
    beta=interpseries(tenorsyrs, beta_list,T)
    rho=interpseries(tenorsyrs, rho_list,T)
    vv=interpseries(tenorsyrs, vv_list,T) 
    
    sabr = [alpha,beta,rho,vv ]

    return sabr












    # This one Calculates the SABR Vol for a single Expiry and strike

def SABR(alpha, beta, rho, nu, F, K, time, MKT):  # all variables are scalars

    if K <= 0:  # negative rates' problem, need to shift the smile
        VOL = 0
        diff = 0
    elif F == K:  # ATM formula
        V = (F * K) ** ((1 - beta) / 2.)
        logFK = math.log(F / K)
        A = 1 + (((1 - beta) ** 2 * alpha ** 2) / (24. * (V ** 2)) + (alpha * beta * nu * rho) / (4. * V) + (
                    (nu ** 2) * (2 - 3 * (rho ** 2)) / 24.)) * time
        B = 1 + (1 / 24.) * (((1 - beta) * logFK) ** 2) + (1 / 1920.) * (((1 - beta) * logFK) ** 4)
        VOL = (alpha / V) * A
        diff = VOL - MKT
    elif F != K:  # not-ATM formula
        V = (F * K) ** ((1 - beta) / 2.)
        logFK = math.log(F / K)
        z = (nu / alpha) * V * logFK
        x = math.log((math.sqrt(1 - 2 * rho * z + z ** 2) + z - rho) / (1 - rho))
        A = 1 + (((1 - beta) ** 2 * alpha ** 2) / (24. * (V ** 2)) + (alpha * beta * nu * rho) / (4. * V) + (
                    (nu ** 2) * (2 - 3 * (rho ** 2)) / 24.)) * time
        B = 1 + (1 / 24.) * (((1 - beta) * logFK) ** 2) + (1 / 1920.) * (((1 - beta) * logFK) ** 4)
        VOL = (nu * logFK * A) / (x * B)
        diff = VOL - MKT

    return VOL


    # This one Calculates the SABR Vol Curve for a single Expiry

def smile(alpha, beta, rho, nu, F, K, time, MKT):  # F, time and the parameters are scalars, K and MKT are vectors
    vol = []
    for j in range(len(K)):
        if K[0] <= 0:
            shift(F, K)
        vol.append(SABR(alpha, beta, rho, nu, F, K[j], time, MKT[j]))

    return vol


# ##### OPTIMISATION OBJECT AND FUNCTION   ###############


# This one creates the optimisation object
def objfunc(par, F, K, time, MKT):
    sum_sq_diff = 0
    # if K[0]<=0:
    #    shift(F,K)
    for j in range(len(K)):
        if MKT[j] == 0:
            diff = 0
        elif F == K[j]:
            V = (F * K[j]) ** ((1 - par[1]) / 2.)
            logFK = math.log(F / K[j])
            A = 1 + (((1 - par[1]) ** 2 * par[0] ** 2) / (24. * (V ** 2)) + (par[0] * par[1] * par[3] * par[2]) / (
                        4. * V) + ((par[3] ** 2) * (2 - 3 * (par[2] ** 2)) / 24.)) * time
            B = 1 + (1 / 24.) * (((1 - par[1]) * logFK) ** 2) + (1 / 1920.) * (((1 - par[1]) * logFK) ** 4)
            VOL = (par[0] / V) * A
            diff = VOL - MKT[j]
        elif F != K[j]:
            V = (F * K[j]) ** ((1 - par[1]) / 2.)
            logFK = math.log(F / K[j])
            z = (par[3] / par[0]) * V * logFK
            x = math.log((math.sqrt(1 - 2 * par[2] * z + z ** 2) + z - par[2]) / (1 - par[2]))
            A = 1 + (((1 - par[1]) ** 2 * par[0] ** 2) / (24. * (V ** 2)) + (par[0] * par[1] * par[3] * par[2]) / (
                        4. * V) + ((par[3] ** 2) * (2 - 3 * (par[2] ** 2)) / 24.)) * time
            B = 1 + (1 / 24.) * (((1 - par[1]) * logFK) ** 2) + (1 / 1920.) * (((1 - par[1]) * logFK) ** 4)
            VOL = (par[3] * logFK * A) / (x * B)
            diff = VOL - MKT[j]
        sum_sq_diff = sum_sq_diff + diff ** 2
        obj = math.sqrt(sum_sq_diff)
    return obj

    # This does the actual optimisation of parameters on the above OBJECT
    # NOTE THE bnds - boundary conditions, where can SET BETA = 1


def calibration(starting_par, F, K, time, MKT, fixbeta):
    x0 = starting_par
    bnds = ((0.001, None), (fixbeta, 1), (-0.999, 0.999), (0.001, None))  # FIX BETA TO 1 HERE
    res = minimize(objfunc, x0, (F, K, time, MKT), bounds=bnds,
                   method='SLSQP')  # for a constrained minimization of multivariate scalar functions
    alpha = res.x[0]
    beta = res.x[1]
    rho = res.x[2]
    nu = res.x[3]
    paramlist = [alpha, beta, rho, nu]
    return paramlist



###====================   SABR WING FUNCTIONS====================

# METHOD 1 - SABR STRETCH
#### Returns adjusted strikes for use in new vol calcs
def stretchfactor(K, F, T, Ps, Cs, Sdsrange, Sdsrange2, ATMV, outputtype,sigmoidfactor ):
    normstrike = []
    newnormstrike = []
    newabsstrike = []
    Psmod = []
    for k in range(len(K)):
        normstrike.append(np.log(K[k] / F[k]) / (np.sqrt(T[k]) * ATMV))
        if abs(normstrike[k]) <= Sdsrange:  # ATMs, no adjustments just raw SABR Fit
            Psmod.append(1.00)
            newnormstrike.append(normstrike[k])
        elif normstrike[k] <= -Sdsrange2:  # teenie puts outside blend, full stretch
            Psmod.append(Ps)
            newnormstrike.append(-Sdsrange + (normstrike[k] - (-Sdsrange)) * Ps)
        elif normstrike[k] >= Sdsrange2:  # teenie calls outside blend, full stretch
            Psmod.append(Cs)
            newnormstrike.append(Sdsrange + (normstrike[k] - (Sdsrange)) * Cs)
        elif ((normstrike[k] <= -Sdsrange) and (normstrike[k] >= -Sdsrange2)):  # Puts in sigmoid range
            # Scale Ps based on normstrike to blend
            Psmod.append(1 + (Ps - 1) * (1 / (1 + (((abs(normstrike[k]) - Sdsrange) / (Sdsrange2 - Sdsrange)) / (
                        1 - (abs(normstrike[k]) - Sdsrange) / (Sdsrange2 - Sdsrange))) ** -sigmoidfactor)))
            # Psmod.append(5.0)
            newnormstrike.append(-Sdsrange + (normstrike[k] - (-Sdsrange)) * Psmod[k])
        elif ((normstrike[k] <= Sdsrange2) and (normstrike[
                                                    k] >= Sdsrange)):  # Calls in sigmoid range                                     # Calls in sigmoid range
            # Scale Cs based on normstrike to blend
            Psmod.append(1 + (Cs - 1) * (1 / (1 + (((abs(normstrike[k]) - Sdsrange) / (Sdsrange2 - Sdsrange)) / (
                        1 - (abs(normstrike[k]) - Sdsrange) / (Sdsrange2 - Sdsrange))) ** -sigmoidfactor)))
            newnormstrike.append(Sdsrange + (normstrike[k] - (Sdsrange)) * Cs)
        else:
            Psmod.append(0.00)
            newnormstrike.append(normstrike[k])
        newabsstrike.append(F[k] * np.exp(newnormstrike[k] * ATMV * (np.sqrt(T[k]))))
   # print(Psmod)
    if outputtype == "K":
        return newabsstrike
    else:
        return Psmod

# METHOD 3 - NEW WING FUNCTION BASED ON SIGMOIDS

def sigmoidfunc(K, F, T, Pf, Cf, xpower, SDR1, SDR2, ATMV):
    normstrike = []
    volfactor = []
    power = xpower
    #  WE ARE MULTIPLYING THE Pf and Cf by abs(normstrike)-SDR1 to give a gradient increase ie y = Pf * x
    #  DONT WANT GRADIENT INCREASE TO START UNTIL AFTER SD1

    # ADDING IN A STRETCHING FACTOR HERE - CAN ADD AS INPUT LATER POSSIBLY
    stretcher = 1  # THIS MAKES IT A 2 SD Point for max vol stretch
    for k in range(len(K)):
        normstrike.append((1 / stretcher) * np.log(K[k] / F[k]) / (ATMV[k] * np.sqrt(T[k])))

        if abs(normstrike[k]) <= SDR1:  # ATM range
            volfactor.append(1.00)
        elif normstrike[k] <= -SDR2:  # teenie puts outside sigoid range
            volfactor.append(1 + Pf * (abs(normstrike[k]) - SDR1))
        elif normstrike[k] >= SDR2:  # teenie calls outside sigmoid range
            volfactor.append(1 + Cf * (abs(normstrike[k]) - SDR1))
        elif ((normstrike[k] <= -SDR1) and (normstrike[k] >= -SDR2)):  # Puts in sigmoid range
            volfactor.append(1 + (Pf * (abs(normstrike[k]) - SDR1)) * (1 / (1 + (
                        ((abs(normstrike[k]) - SDR1) / (SDR2 - SDR1)) / (
                            1 - (abs(normstrike[k]) - SDR1) / (SDR2 - SDR1))) ** -power)))
        elif ((normstrike[k] <= SDR2) and (normstrike[k] >= SDR1)):  # Calls in sigmoid range
            volfactor.append(1 + (Cf * (abs(normstrike[k]) - SDR1)) * (1 / (1 + (
                        ((abs(normstrike[k]) - SDR1) / (SDR2 - SDR1)) / (
                            1 - (abs(normstrike[k]) - SDR1) / (SDR2 - SDR1))) ** -power)))
        else:
            volfactor.append(1.00)
    return volfactor





### QUAD FUNCTIONS

def singlepointquad(F, K, ATMV, Skew, PConv, CConv, T):
    adjSkew = Skew / 10
    normstrike = np.log(K / F) / np.sqrt(T)
    if normstrike <= 0:
        ivol = ATMV + (adjSkew * normstrike) + PConv * abs(normstrike) * abs(normstrike)
    else:
        ivol = ATMV + (adjSkew * normstrike) + CConv * abs(normstrike) * abs(normstrike)
    return ivol


def basicquad(F, K, ATMV, Skew, PConv, CConv, T):
    adjSkew = Skew / 10
    normstrike = np.log(K / F) / np.sqrt(T)
    for k in range(len(K)):
        if normstrike[k] <= 0:
            ivol = ATMV + (adjSkew * normstrike[k]) + PConv * abs(normstrike[k]) * abs(normstrike[k])
        else:
            ivol = ATMV + (adjSkew * normstrike[k]) + CConv * abs(normstrike[k]) * abs(normstrike[k])
    return ivol


def basicquadATMstrikes(F, K, ATMV, Skew, PConv, CConv, T, SD):
    # Use SD cutoffs to pick ATM strike range only for SABR Fit
    normTTEStrike = np.log(K / F) / (np.sqrt(T) * ATMV)
    adjSkew = Skew / 10
    normstrike = np.log(K / F) / np.sqrt(T)
    ivol = []
    for k in range(len(K)):
        if abs(normTTEStrike[k]) > SD:
            ivol.append(0)
        elif normTTEStrike[k] <= 0:
            ivol.append(ATMV[k] + (adjSkew * normstrike[k]) + PConv * abs(normstrike[k]) * abs(normstrike[k]))
        elif normTTEStrike[k] > 0:
            ivol.append(ATMV[k] + (adjSkew * normstrike[k]) + CConv * abs(normstrike[k]) * abs(normstrike[k]))
    return ivol



#=====================================   SABR FITTING ============================================================================


#Gives SABR params for a single Expiry Fit - df needs 2 cols 'Strike' & 'Vol'
def FitSABRparams(df, fwd, tyrs, Kmin, Kmax, Salpha,Srho,Snu  ):

    starting_guess = np.array([Salpha, 1.0, Srho, Snu])
    alpha = starting_guess[0]
    beta = starting_guess[1]
    rho = starting_guess[2]
    nu = starting_guess[3]
    fwdvalue = fwd
    timevalue = tyrs
    FixBetato1 = 1  # lower bound for beta in ( X, 1) so if set to 1 then Beta =1 fixed, if set to 0 then fit Beta
    def strikefilter(Kmin, Kmax, K, MktV):
        vols = []
        for k in range(len(K)):
            if Kmin <= K[k] <= Kmax:
                vols.append(MktV[k])
            else:
                vols.append(0)
        return vols
    df["MktVolAdj"] = 0
    df["MktVolAdj"] = strikefilter(Kmin, Kmax, df["Strike"], df["Vol"])
    ##############      CALIBRATE STANDARD SABR TO ATM STRIKES     #################
    # define list to hold calibration output
    fitresult = []
    fitresult = calibration(starting_guess, fwdvalue, df["Strike"], timevalue, df["MktVolAdj"], FixBetato1)
    
    return fitresult



#####  CALC QUAD PARAMS FROM 5 points on the SABR FIT, (ATM,99.5/100.5 and +-1SD) - gives ATMFVol, Skew, PutConv, CallConv
def quadfromSABR(SABRparamlist, fwd, tyrs):
    fitresult=SABRparamlist
    fwdvalue=fwd
    timevalue=tyrs
    results=[]
    # Calc vol at ATMF
    ATMFvol = SABR(fitresult[0], fitresult[1], fitresult[2], fitresult[3], fwdvalue, fwdvalue, timevalue, 0.15)
    # Calc vol at 99.5 and 100.5 to back out dydx skew gradient
    minushalfvol = SABR(fitresult[0], fitresult[1], fitresult[2], fitresult[3], fwdvalue, fwdvalue * 0.995, timevalue, 0.15)
    plushalfvol = SABR(fitresult[0], fitresult[1], fitresult[2], fitresult[3], fwdvalue, fwdvalue * 1.005, timevalue, 0.15)
    dydx = (minushalfvol - plushalfvol) / (
                np.log(fwdvalue * 0.995 / fwdvalue) / np.sqrt(timevalue) - np.log(fwdvalue * 1.005 / fwdvalue) / np.sqrt(
            timevalue))
    # Calc vol at 1.0SD OF ATMFVols for CONVEXITY
    sdsconvex = 1.0
    lowconvstrike = fwdvalue * np.exp(-sdsconvex * ATMFvol * np.sqrt(timevalue))
    highconvstrike = fwdvalue * np.exp(sdsconvex * ATMFvol * np.sqrt(timevalue))
    lowconvvol = SABR(fitresult[0], fitresult[1], fitresult[2], fitresult[3], fwdvalue, lowconvstrike, timevalue, 0.15)
    highconvvol = SABR(fitresult[0], fitresult[1], fitresult[2], fitresult[3], fwdvalue, highconvstrike, timevalue, 0.15)
    d2ydx2put = (lowconvvol - ATMFvol - (np.log(lowconvstrike / fwdvalue) / np.sqrt(timevalue) * dydx)) / (
                abs(np.log(lowconvstrike / fwdvalue) / np.sqrt(timevalue)) * abs(
            np.log(lowconvstrike / fwdvalue) / np.sqrt(timevalue)))
    d2ydx2call = (highconvvol - ATMFvol - (np.log(highconvstrike / fwdvalue) / np.sqrt(timevalue) * dydx)) / (
                abs(np.log(highconvstrike / fwdvalue) / np.sqrt(timevalue)) * abs(
            np.log(highconvstrike / fwdvalue) / np.sqrt(timevalue)))
    results.append(ATMFvol)
    results.append(dydx)
    results.append(d2ydx2put)
    results.append(d2ydx2call)
    return results



#####  CALC SABR PARAMETERS FROM QUAD INPUTS -------
def SABRparamsfromquad(ATMvol, skew, pconv, cconv, fwd, tyrs):
    starting_guess = np.array([ATMvol, 1.0, -0.8, 1])
    sdsconvex = 1.0
    lowconvstrike = fwd * np.exp(-sdsconvex * ATMvol * np.sqrt(tyrs))
    highconvstrike = fwd * np.exp(sdsconvex * ATMvol * np.sqrt(tyrs))
    fitstrike = [lowconvstrike, fwd, highconvstrike]
    putvol = singlepointquad(fwd, lowconvstrike, ATMvol, skew, pconv, cconv, tyrs)
    callvol = singlepointquad(fwd, highconvstrike, ATMvol, skew, pconv, cconv, tyrs)
    fitvols = [putvol, ATMvol, callvol]
    fitdf = pd.DataFrame(list(zip(fitstrike, fitvols)), columns=["Strike", "Vol"])
    # -------  FIT SABR PARAMS TO THE 3 QUAD POINTS (ATM, +-1SD)  
    fit3ptresult = calibration(starting_guess, fwd, fitdf["Strike"], tyrs, fitdf["Vol"], 1)
    return fit3ptresult





#===================================== OPTION PRICING FUNCTIONS ====================================================================================

def fwdpricecalc1(Spot,Rate, Div, T):
    if np.isnan([Spot,Rate, Div, T]).any():
        Fwdprice=np.nan
    else:
        Fwdprice=Spot*np.exp((Rate-Div)*T)
    return Fwdprice


def divyieldcalc(Spot,Rate, Fwd, T):

    divyield=Rate - math.log(Fwd/Spot)/T
    return divyield



def strikefromdelta(delt, spot , t , vol , rf , div , cp ):

    if cp == "C":
        callstrike = spot / (np.exp((vol * np.sqrt(t)) * norm.ppf(delt / np.exp(-div * t)) - (rf - div + 0.5 * vol ** 2) * t))
        outputholder = callstrike
    
    elif cp == "P":
        putstrike = spot / (np.exp((vol * np.sqrt(t)) * norm.ppf((delt / np.exp(-div * t)) + 1) - (rf - div + 0.5 * vol ** 2) * t))
        outputholder = putstrike

    return outputholder


# Define black Scholes model
def black_scholes(s, k, t, v, rf, div, cp, type, undisc):
    d1 = (math.log(s / k) + (rf - div + 0.5 * math.pow(v, 2)) * t) / (v * math.sqrt(t))
    d2 = d1 - v * math.sqrt(t)
    NPrime = ((2 * np.pi) ** (-1 / 2)) * np.exp(-0.5 * (d1) ** 2)

    # IF ITS A CALL
    if cp == 1:
        if type == "P":
            optprice = cp * s * math.exp(-div * t) * stats.norm.cdf(cp * d1) - cp * k * math.exp(
                -rf * t) * stats.norm.cdf(cp * d2)
            if undisc==1:
                result = optprice *math.exp(rf * t)
            else:
                result = optprice

        elif type == "D":
            # Calc Option Greeks
            calldelta = math.exp(-div * t) * stats.norm.cdf(d1)
            result = calldelta
        elif type == "G":
            optioncallgamma = math.exp(-div * t) * (NPrime / (s * v * t ** (1 / 2)))
            result = optioncallgamma
        elif type == "V":
            optioncallvega = (1 / 100) * s * math.exp(-div * t) * t ** (1 / 2) * NPrime
            result = optioncallvega
        elif type == "T":
            optioncalltheta = (1 / 260) * (
                        NPrime * math.exp(-div * t) * (-s * v * 0.5 / np.sqrt(t)) - rf * k * np.exp(
                    -rf * t) * stats.norm.cdf(d2) + div * s * math.exp(-div * t) * stats.norm.cdf(d1))
            result = optioncalltheta
        elif type == "R":
            optioncallrho = k * t * math.exp(-rf * t) * stats.norm.cdf(d2) / 100
            result = optioncallrho
        else:
            print("Error")
    elif cp == -1:  # THIS IS THE PUTS
        if type == "P":
            optprice = cp * s * math.exp(-div * t) * stats.norm.cdf(cp * d1) - cp * k * math.exp(
                -rf * t) * stats.norm.cdf(cp * d2)
            if undisc==1:
                result = optprice *math.exp(rf * t)
            else:
                result = optprice
        elif type == "D":
            # Calc Option Greeks
            putdelta = math.exp(-div * t) * (stats.norm.cdf(d1) - 1)
            result = putdelta
        elif type == "G":
            optionputgamma = math.exp(-div * t) * (NPrime / (s * v * t ** (1 / 2)))
            result = optionputgamma
        elif type == "V":
            optionputvega = (1 / 100) * s * math.exp(-div * t) * t ** (1 / 2) * NPrime
            result = optionputvega
        elif type == "T":
            optionputtheta = (1 / 260) * (
                        NPrime * math.exp(-div * t) * (-s * v * 0.5 / np.sqrt(t)) + rf * k * np.exp(
                    -rf * t) * stats.norm.cdf(-d2) - div * s * math.exp(-div * t) * stats.norm.cdf(-d1))
            result = optionputtheta
        elif type == "R":
            optionputrho = -k * t * math.exp(-rf * t) * stats.norm.cdf(-d2) / 100
            result = optionputrho
        else:
            print("Error")
    else:  # THIS IS THE FORWARDS, CP=0, calc as a synthetic for price????
        if type == "P":
            fwdprice = (s * math.exp(-div * t) * stats.norm.cdf(d1) - k * math.exp(-rf * t) * stats.norm.cdf(
                d2)) - ((-1) * s * math.exp(-div * t) * stats.norm.cdf(-1 * d1) - (-1) * k * math.exp(
                -rf * t) * stats.norm.cdf(-1 * d2))
            if undisc==1:
                result = fwdprice *math.exp(rf * t)
            else:
                result = fwdprice
        elif type == "D":
            # Calc Forward Delta by perturbation:
            forwardexpiry = (datetime.datetime.strptime(forwarddate.get(), '%d/%m/%Y')).date()
            spot = float(spotinput.get())
            rate = float(rateinput.get()) / 100
            div = float(divinput.get()) / 100
            today = date.today()
            timeyears = (forwardexpiry - today).days / 365
            fwddelta = 100 * (spot * 1.005 * math.exp((rate - div) * timeyears) - spot * 0.995 * math.exp(
                (rate - div) * timeyears)) / spot
            result = fwddelta
        elif type == "G":
            fwdgamma = math.exp(-div * t) * (NPrime / (s * v * t ** (1 / 2))) - (
                        math.exp(-div * t) * (NPrime / (s * v * t ** (1 / 2))))
            result = fwdgamma
        elif type == "V":
            fwdvega = (1 / 100) * s * math.exp(-div * t) * t ** (1 / 2) * NPrime - (
                        (1 / 100) * s * math.exp(-div * t) * t ** (1 / 2) * NPrime)
            result = fwdvega
        elif type == "T":
            fwdtheta = (1 / 260) * (NPrime * math.exp(-div * t) * (-s * v * 0.5 / np.sqrt(t)) - rf * k * np.exp(
                -rf * t) * stats.norm.cdf(d2) + div * s * math.exp(-div * t) * stats.norm.cdf(d1)) - ((1 / 260) * (
                        NPrime * math.exp(-div * t) * (-s * v * 0.5 / np.sqrt(t)) + rf * k * np.exp(
                    -rf * t) * stats.norm.cdf(-d2) - div * s * math.exp(-div * t) * stats.norm.cdf(-d1)))
            result = fwdtheta
        elif type == "R":
            fwdrho = k * t * math.exp(-rf * t) * stats.norm.cdf(d2) / 100 - (
                        -k * t * math.exp(-rf * t) * stats.norm.cdf(-d2) / 100)
            result = fwdrho
        else:
            print("Error")

    return result



#Display Option screen for dataframe
def OptionScreen(df):
    # Calc TheoValues from the WingSABRmodel
    calltv = []
    puttv = []

    for k in range(len(df["Strike"])):
        # black_scholes(s,k,t,v,rf,div,cp,type)
        calltv.append(black_scholes(df["Spot"][k], df["Strike"][k], df["Time"][k],
                                    df["FitWingSABRtoQuadVol"][k], (df["RFrate"][k])/100, (df["ImpDivyield"][k])/100,
                                    1, "P",0))
        puttv.append(black_scholes(df["Spot"][k], df["Strike"][k], df["Time"][k],
                                   df["FitWingSABRtoQuadVol"][k], (df["RFrate"][k])/100, (df["ImpDivyield"][k])/100,
                                   -1, "P",0))
    # asign TVs to columns and round to 2dp
    df["Call TV"] = [round(elem, 2) for elem in calltv]
    df["Put TV"] = [round(elem, 2) for elem in puttv]
    optionscreen = df[["Strike", "Call Bid", "Call TV", "Call Ask", "Put Bid", "Put TV", "Put Ask"]].copy(deep=True)
    return optionscreen






#===================================================  VARSWAP FUNCTIONS ==============================================================

def fwdvariance(Nt0, Nt1, var1, var2):

    fwdvol = np.sqrt(((-Nt0/(Nt1-Nt0))*var1**2)+((Nt1/(Nt1-Nt0))*var2**2))
    return fwdvol

def varunits(strike, vega):

    varunits=vega/(2*strike)
    return varunits

def vegaamount(strike, varunits):

    vegaamt=(2*strike)*varunits
    return vegaamt

def varpnl(strike, varunits, vollevel):

    varpnlnew=varunits*(vollevel**2-strike**2)
    return varpnlnew

def varunitgamma(strike, Nexp):
    if Nexp>0:
        vargamma=(2*strike*252) / ((strike/100)*Nexp)
    else:
        vargamma=0
    return vargamma

def varunittheta(strike,  Nexp):
    if Nexp>0:
        vartheta=(1/Nexp) * (-(strike**2))
    else:
        vartheta=0
    return vartheta

def volswappnl(strike, vega, vollevel):

    volswappnl=vega*(vollevel-strike)
    return volswappnl





###============================================CALC PDFs =====================================================================================

# CALC PDF FROM SABRSTRETCH===============================
def ImpliedPDFfromSABRstretch(spot, fwd, tyrs, SABRparamlist, putstretch, callstretch, SDrange, SDrange2, Vol, sigmoidfactor):
    #Create a list of strikes 0.5% of the Fwd wide from 10% to 200%
    PDFdf=pd.DataFrame()
    lowstrike=0.1
    highstrike=2.0
    deltastrike=0.005
    pdfstrikes=[]
    lenofk=int(3+(highstrike-lowstrike)/deltastrike)
    for i in range(0,lenofk):
        pdfstrikes.append(lowstrike-deltastrike + i*deltastrike)
    PDFdf['Fwdstrikepct']=pdfstrikes
    PDFdf['Spot']=spot
    PDFdf['Forward']=fwd
    PDFdf['Time']=tyrs
    PDFdf['RFrate']=tyrs
    PDFdf['ImpDivyield']=tyrs
    PDFdf['absstrike']=PDFdf['Fwdstrikepct']*PDFdf['Forward'] 
    PDFdf["StretchedStrike"] = stretchfactor(PDFdf['absstrike'], PDFdf['Forward'], PDFdf['Time'], putstretch,
                                                callstretch, SDrange, SDrange2, Vol, "K", sigmoidfactor)   
    #calc the vols USING STRETCHED WING MODEL VARIANT
    PDFdf['Vol']=smile(SABRparamlist[0], SABRparamlist[1], SABRparamlist[2], SABRparamlist[3],
                                                 fwd, PDFdf["StretchedStrike"], tyrs, PDFdf['Time'])
    #Calc Undiscounted Fly at each point
    calllist=[]
    for k in range(0, len(PDFdf)):
        calllist.append(black_scholes(PDFdf["Spot"][k], PDFdf["absstrike"][k], PDFdf["Time"][k],
                                    PDFdf["Vol"][k], (PDFdf["RFrate"][k])/100, (PDFdf["ImpDivyield"][k])/100,
                                    1, "P",1))
    flylist=[]
    flylist.append(0)
    for k in range(1, len(PDFdf)-1):
        flylist.append(calllist[k-1] + calllist[k+1] -2*calllist[k] )
    flylist.append(0)
    PDFdf['Call']=calllist
    PDFdf['Fly']=flylist
    PDFdf['PDF']=PDFdf['Fly']/(PDFdf["Forward"]*deltastrike)
    PDFdf['CDF']=PDFdf['PDF'].cumsum()    
    PDFdf['PDF1%']=PDFdf['PDF'] * 0.01/deltastrike
    output=PDFdf[['Fwdstrikepct','PDF1%', 'CDF']].copy()
    return output



# CALC PDF FROM SABRSIGMOID==============================
def ImpliedPDFfromSABRsigmoid(spot, fwd, tyrs, SABRparamlist, putmult, callmult, Betafactor, SD1, SD2, Vol):
    #Create a list of strikes 0.5% of the Fwd wide from 10% to 200%
    PDFdf=pd.DataFrame()
    lowstrike=0.1
    highstrike=2.0
    deltastrike=0.005
    pdfstrikes=[]
    lenofk=int(3+(highstrike-lowstrike)/deltastrike)
    for i in range(0,lenofk):
        pdfstrikes.append(lowstrike-deltastrike + i*deltastrike)
    PDFdf['Fwdstrikepct']=pdfstrikes
    PDFdf['Spot']=spot
    PDFdf['Forward']=fwd
    PDFdf['Time']=tyrs
    PDFdf['RFrate']=tyrs
    PDFdf['ImpDivyield']=tyrs
    PDFdf['ATMV']=Vol
    PDFdf['absstrike']=PDFdf['Fwdstrikepct']*PDFdf['Forward'] 
    PDFdf["ExpFactor"] = sigmoidfunc(PDFdf['absstrike'], PDFdf['Forward'], PDFdf['Time'], putmult, callmult,
                                        Betafactor, SD1, SD2, PDFdf['ATMV'])
    # Scale up the SABR vol by the ExpFactor
    PDFdf['Vol']=smile(SABRparamlist[0], SABRparamlist[1], SABRparamlist[2], SABRparamlist[3],
                                                 fwd, PDFdf['absstrike'], tyrs, PDFdf['Time'])* PDFdf['ExpFactor']
    #Calc Undiscounted Fly at each point
    calllist=[]
    for k in range(0, len(PDFdf)):
        calllist.append(black_scholes(PDFdf["Spot"][k], PDFdf["absstrike"][k], PDFdf["Time"][k],
                                    PDFdf["Vol"][k], (PDFdf["RFrate"][k])/100, (PDFdf["ImpDivyield"][k])/100,
                                    1, "P",1))
    flylist=[]
    flylist.append(0)
    for k in range(1, len(PDFdf)-1):
        flylist.append(calllist[k-1] + calllist[k+1] -2*calllist[k] )
    flylist.append(0)
    PDFdf['Call']=calllist
    PDFdf['Fly']=flylist
    PDFdf['PDF']=PDFdf['Fly']/(PDFdf["Forward"]*deltastrike)
    PDFdf['CDF']=PDFdf['PDF'].cumsum()    
    PDFdf['PDF1%']=PDFdf['PDF'] * 0.01/deltastrike
    output=PDFdf[['Fwdstrikepct','PDF1%', 'CDF']].copy()
    return output


#CALCS THE BUCKETED PDF OF IMPLIED AS DATAFRAME
def BucketImpliedBarDF( df):
    bardf=pd.DataFrame()
    bucketmids=[0.675, 0.725, 0.775, 0.825, 0.875, 0.925, 0.975, 1.025, 1.075, 1.125, 1.175, 1.225, 1.275, 1.325]
    bucketstart=[0.7,   0.75,  0.80, 0.85,   0.9,   0.95,  1.0,   1.05,  1.1,   1.15,  1.2,  1.25, 1.3, 2.0]
    bucketlabels=['<70', '70-75','75-80','80-85','85-90','90-95','95-100','100-105','105-110','110-115','115-120','120-125','125-130','>130'    ]
    probabilities=[]
    for i in range(0,len(bucketstart)):
        probabilities.append(df.loc[df['Fwdstrikepct']<= bucketstart[i]]['PDF1%'].sum() * 0.005/0.01 )    
    histo=[]
    histo.append(probabilities[0])
    for j in range(1, len(probabilities)):
        histo.append(probabilities[j]-probabilities[j-1])     
    bardf['Bucketmids']=bucketmids
    bardf['histo']=histo
    bardf['Bucketname']=bucketlabels
    return bardf



# PDF CHART CLASS ==========================================
class MyPDFChart():
    def __init__(self):
        pass
    #CHARTS THE PDF AND CDF OF IMPLIED
    def createmychart(self, df, savelabel):
        # OUTPUT CHART
        xseries='Fwdstrikepct'
        y1seriesone='CDF'
        y2seriesone='PDF1%'
        filldataseries='PDF1%'
        fillcolour='grey'
        fillalpha=0.4
        ylabel1="CDF"
        ylabel2="PDF"
        plt.style.use('dark_background')
        figure = plt.Figure(figsize=(9, 6), dpi=100)
        ax1 = figure.add_subplot(111)
        ax1 = df.plot(x=xseries, y=y1seriesone)
        ax3 = ax1.twinx()
        plt.fill_between(df[xseries], df[filldataseries], color=fillcolour, alpha=fillalpha)
        df.plot(x=xseries, y=y2seriesone, ax=ax3, linestyle='dashed', color='red', alpha=0.7, legend=True)
        df.plot(x=xseries, y=filldataseries, ax=ax3, color=fillcolour, alpha=fillalpha, legend=False)
        ax1.set_ylabel(ylabel1)
        ax3.set_ylabel(ylabel2)
        ax1.legend(loc='upper left')
        ax3.legend(loc='upper right')
        ax1.grid(b=False, which='major', axis='y')
        ax1.grid(b=False, which='major', axis='x')
        ax3.grid(b=False, which='major', axis='y')
        plt.savefig(savelabel)
        plt.show()
    
    #CHARTS THE BUCKETED PDF OF IMPLIED
    def BucketImpliedBarChart(self, df, title, savelabel):
        xaxis='Bucketname'
        yaxis='histo'
        xaxislable='SpotBucket'
        yaxislabel='Probability'
        bardf=pd.DataFrame()
        bucketmids=[0.675, 0.725, 0.775, 0.825, 0.875, 0.925, 0.975, 1.025, 1.075, 1.125, 1.175, 1.225, 1.275, 1.325]
        bucketstart=[0.7,   0.75,  0.80, 0.85,   0.9,   0.95,  1.0,   1.05,  1.1,   1.15,  1.2,  1.25, 1.3, 2.0]
        bucketlabels=['<70', '70-75','75-80','80-85','85-90','90-95','95-100','100-105','105-110','110-115','115-120','120-125','125-130','>130'    ]
    
        probabilities=[]
        for i in range(0,len(bucketstart)):
            probabilities.append(df.loc[df['Fwdstrikepct']<= bucketstart[i]]['PDF1%'].sum() * 0.005/0.01 )
        
        histo=[]
        histo.append(probabilities[0])
        for j in range(1, len(probabilities)):
            histo.append(probabilities[j]-probabilities[j-1])
        
        bardf['Bucketmids']=bucketmids
        bardf['histo']=histo
        bardf['Bucketname']=bucketlabels
    
        # OUTPUT CHART
        plt.style.use('dark_background')
        fig = plt.figure(figsize=(9, 6), dpi=100)
        ax1 = fig.add_subplot(111)
        # get unique values from the dataframecols
        xuniques = bardf[xaxis].unique()
        _x = np.arange(len(xuniques))
    
        X = np.arange(len(bardf))
        plt.bar(X + 0.00, bardf[yaxis], color='b', width=0.25, label=yaxislabel)
    
        ax1.set_xticks(_x)
        plt.xticks(rotation=90)
        ax1.set_xticklabels(xuniques)
    
        ax1.set_xlabel(xaxislable)
        ax1.set_ylabel(yaxislabel)
        ax1.set_title(title)
        ax1.legend(bbox_to_anchor=(1, 1), loc='upper right', borderaxespad=0., fontsize=10)
        plt.subplots_adjust(bottom=0.3)
        plt.savefig("C:/PricingModels/bar"+savelabel+".png")
        plt.show()
        
        
         
    def BucketHistoricandImpliedBarChart(self, impliedbardf, Underlyer, TRUnderlyer, timevalue, tenorM, historics_start_date,  title, savelabel ):
        histdata=pd.DataFrame
        bucketmids=[0.675, 0.725, 0.775, 0.825, 0.875, 0.925, 0.975, 1.025, 1.075, 1.125, 1.175, 1.225, 1.275, 1.325]
        bucketstart=[0.7,   0.75,  0.80, 0.85,   0.9,   0.95,  1.0,   1.05,  1.1,   1.15,  1.2,  1.25, 1.3, 2.0]
        bucketlabels=['<70', '70-75','75-80','80-85','85-90','90-95','95-100','100-105','105-110','110-115','115-120','120-125','125-130','>130'    ]
        if Underlyer=='SX5E':
            rate='S0045Z '+tenorM+' BLC2 Curncy'
            #histdata['Spot']=histdata['SX5T Index']
            #histdata['RFrate']=histdata[rate]
        elif Underlyer=='SPX':
            rate = 'S0023Z ' + tenorM + ' BLC2 Curncy'
            #histdata['Spot'] = histdata['SPTR500N Index']
           # histdata['RFrate']=histdata[rate]
            
        histdata=blp.bdh(tickers=TRUnderlyer + " Index", flds="PX_LAST",start_date=historics_start_date)
        histdata.columns = histdata.columns.to_flat_index()
        histdata=histdata.rename(columns={(TRUnderlyer + " Index",'PX_LAST'): 'Spot'})
        histdata['RFrate']=blp.bdh(tickers=rate, flds="PX_LAST",start_date=historics_start_date).iloc[:,0]
        histdata["Spot"] = pd.to_numeric(histdata["Spot"], errors='coerce')
        histdata['RFrate']=pd.to_numeric(histdata["RFrate"], errors='coerce')
        histdata['Time']=timevalue
        histdata['ImpDivyield']=0.01
        histdata['Vol']=0.2
        synthcall=[]
        synthput=[]
        for k in range(0, len(histdata)):
            synthcall.append(black_scholes(histdata["Spot"][k], histdata["Spot"][k], histdata["Time"][k],
                                        histdata["Vol"][k], (histdata["RFrate"][k])/100, histdata["ImpDivyield"][k],
                                        1, "P",1))
            synthput.append(black_scholes(histdata["Spot"][k], histdata["Spot"][k], histdata["Time"][k],
                                       histdata["Vol"][k], (histdata["RFrate"][k]) / 100, histdata["ImpDivyield"][k],
                                       -1, "P", 1))
        histdata['SynthCall']=synthcall
        histdata['SynthPut']=synthput
        histdata['SynthLevel']=histdata["Spot"]+histdata['SynthCall']-histdata['SynthPut']
        tradedays=int(histdata['Time'][0]*252*-1)
        histdata['Expiry']=histdata["Spot"].shift(tradedays)
        histdata.dropna(inplace=True)
        histdata['Return']=histdata["Expiry"]/histdata['SynthLevel']
    
        #==========Create Bar HistoData DF
        histbardf=pd.DataFrame()
        histprobabilities=[]
        for i in range(0,len(bucketstart)):
            histprobabilities.append(histdata.loc[histdata['Return']<= bucketstart[i]]['Return'].count() / len(histdata))
        histocount=[]
        histocount.append(histprobabilities[0])
        for j in range(1, len(histprobabilities)):
            histocount.append(histprobabilities[j]-histprobabilities[j-1])
        histbardf['Bucketmids']=bucketmids
        histbardf['histofreq']=histocount
        histbardf['Bucketname']=bucketlabels
    
        xaxis='Bucketname'
        yaxis='histo'
        xaxislable='SpotBucket'
        yaxislabel='Probability'
        # OUTPUT CHART
        plt.style.use('dark_background')
        fig = plt.figure(figsize=(9, 6), dpi=100)
        ax1 = fig.add_subplot(111)
        # get unique values from the dataframecols
        xuniques = bucketlabels
        _x = np.arange(len(bucketlabels))
        X = np.arange(len(bucketlabels))
        plt.bar(X + 0.00, impliedbardf[yaxis], color='b', width=0.25, label=yaxislabel)
        plt.bar(X + 0.25, histbardf['histofreq'], color='g', width=0.25, label='Historic')
        ax1.set_xticks(_x)
        plt.xticks(rotation=90)
        ax1.set_xticklabels(xuniques)
        ax1.set_xlabel(xaxislable)
        ax1.set_ylabel(yaxislabel)
        ax1.set_title(title)
        ax1.legend(bbox_to_anchor=(1, 1), loc='upper right', borderaxespad=0., fontsize=10)
        plt.subplots_adjust(bottom=0.3)
        plt.savefig(savelabel+".png")
        plt.show()   


